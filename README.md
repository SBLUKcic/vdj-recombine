# VDJ Recombine (VDJGO)

This software finds V-D-J recombinations in input FASTQ records. `Tags` are given
from V and J regions in the human genome. These tags are searched for in the input
FASTQ records. If a V and J or V and V or J and J region is found, the record
is kept, and sorted/filtered based on the variable region between them.

## Usage

The code can be compiled to a runnable jar file with the `shadowJar` commmand 
through gradle. `./gradlew shadowJar`

### Running the jar.

A list of available options can be found using the help command:

`java -jar vdjgo -help`

Basic running:

`java -jar vdjgo -if XSBF487.35bp.vPE.hs_SBL_VDJ_region_mask_v1.PA.p.fr_del.fastq -m coding -d output`

## Basic Program Structure

Main points.

- `JCommander` for command line parsing.
- `Boyer-Moore` algorithms for fast string searching.
- `Spring` framework uses for dependency injection.
- `BioJava` implemented for basic bioinformatic code.

### CLI

The cli directory holds code for parsing the command line options. It uses `JCommander` 
to do most of the heavy lifting. The result is a `ProgramArgs` option with usable
options.

### Algorithms

Classes for string search algorithms to find the V and J tags in the FASTQ sequences.
This code uses the `Boyer-Moore` algorithm for fast string searching.

### Config

`Spring` set up for dependency injection.

### Combinations

Multithreaded producer-consumer style set up for distributing FASTQ records into 
tag finding classes. 

### Aggregation

Combines found combinations by different criteria such as the variable sequence (D region)

### Filter

Multiple classes for filtering VDJ combinations, such as Unique molecular identifier
(UMI)

### fqStream

Classes for finding particular FASTQ records given a set of IDs.

### Information

Class for building info files while the program is running.

### Readers

Reads in a FASTQ file based on the BioJava `ParseListener` and `StreamListener` classes.

### Regions

Contains code to read in and parse V and J region tags.

### Runners

Handles the flow of the program.

### Serialization

Serializes the output, currently contains code for JSON output only. 

## How it works

![](vdjgo-outline-v3.jpg)