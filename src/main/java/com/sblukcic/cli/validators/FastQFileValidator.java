package com.sblukcic.cli.validators;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;
import lombok.extern.slf4j.Slf4j;
import org.biojava.nbio.genome.io.fastq.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

@Slf4j(topic = "FastQ Validation")
public class FastQFileValidator implements IParameterValidator {

    List<FastqReader> readers = Arrays.asList(
            new IlluminaFastqReader(),
            new SangerFastqReader(),
            new SolexaFastqReader()
    );

    @Override
    public void validate(String name, String value) throws ParameterException {

        Path path = Paths.get(value);

        if(!path.toFile().exists()){
            throw new ParameterException(String.format("Path %s doesn't exist", value));
        }

        boolean isValidFile = false;
        String err = null;
        log.info("Validating input fastq file");
        for(FastqReader reader : readers){
            try {
                reader.stream(new BufferedReader(new InputStreamReader(new FileInputStream(path.toFile()))), fastq -> {});
                isValidFile = true;
            } catch (IOException e) {
                //not in this format!
                err = e.getMessage();
            }
        }

        if(!isValidFile){
            throw new ParameterException(String.format("Error reading requested input file: %s", err));
        }

    }
}
