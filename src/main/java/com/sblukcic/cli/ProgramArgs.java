package com.sblukcic.cli;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.converters.PathConverter;
import com.beust.jcommander.validators.PositiveInteger;
import com.sblukcic.cli.converters.ModeConverter;
import com.sblukcic.cli.validators.FastQFileValidator;
import lombok.Getter;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

@Parameters(separators = "= ")
@Getter
public class ProgramArgs {

    @Parameter(
            names = { "-m", "--mode"},
            description = "Select the mode, signal or coding",
            converter = ModeConverter.class
    )
    private Mode mode = Mode.CODING;

    @Parameter(
            names = { "-if", "--if", "--input-file"},
            description = "Input .fastq file",
            validateWith = FastQFileValidator.class,
            converter = PathConverter.class,
            required = true
    )
    private Path inputFilePath;

    @Parameter(
            names = { "-umi", "--umi"},
            description = "De-duplicate combinations for exact Unique Molecular Identifiers (UMI) matches"
    )
    private boolean checkUMIs = false;

    @Parameter(
            names = {"-w", "--umi-mismatch"},
            description = "Allow for mis-matches when comparing umis",
            validateWith = PositiveInteger.class
    )
    private int allowedUMIMismatches = 0;

    @Parameter(
            names = {"-h", "--h", "-help", "--help"},
            description = "Print help menu, exit",
            help = true
    )
    private boolean help;

    @Parameter(
            names = {"-d", "--dir"},
            description = "Output directory path",
            converter = PathConverter.class
    )
    private Path directory = Paths.get(System.getProperty("user.dir") + File.separator + "vdj_" + System.nanoTime());

}
