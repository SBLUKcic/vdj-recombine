package com.sblukcic.cli;

public interface Parser {

    ProgramArgs parse(String[] args);

}
