package com.sblukcic.cli.converters;

import com.beust.jcommander.IStringConverter;
import com.sblukcic.cli.Mode;

public class ModeConverter implements IStringConverter<Mode> {
    @Override
    public Mode convert(String value) {

        if(value.toLowerCase().equals(Mode.CODING.toString().toLowerCase())){
            return Mode.CODING;
        }

        return Mode.SIGNAL;
    }
}
