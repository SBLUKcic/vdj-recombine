package com.sblukcic.cli;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

public class JCommanderParser implements Parser {

    @Override
    public ProgramArgs parse(String[] args) {

        ProgramArgs pArgs = new ProgramArgs();

        JCommander commander = JCommander
                .newBuilder()
                .addObject(pArgs)
                .build();

        commander.parse(args);

        if(pArgs.isHelp()){
            commander.usage();
            throw new ParameterException("Help Called");
        }

        return pArgs;
    }
}
