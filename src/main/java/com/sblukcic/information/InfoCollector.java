package com.sblukcic.information;

import java.io.IOException;
import java.io.OutputStream;

public interface InfoCollector {

    void addInfo(String title, String... lines);

    void write(OutputStream stream) throws IOException;
}
