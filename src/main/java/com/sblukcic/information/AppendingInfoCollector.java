package com.sblukcic.information;

import lombok.val;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

//If a section already exists, new info will be appended.
public class AppendingInfoCollector implements InfoCollector {

    private Map<String, List<String>> sections = new LinkedHashMap<>();

    @Override
    public void addInfo(String title, String... lines) {
        if(!sections.containsKey(title)){
            sections.put(title, new LinkedList<>());
        }

        for(String line : lines){
            sections.get(title).add(line);
        }
    }

    @Override
    public void write(OutputStream stream) throws IOException {

        try(PrintWriter writer = new PrintWriter(new OutputStreamWriter(stream))){

            for(val entry : sections.entrySet()){
                writer.println();
                writer.println(String.format("\t%s\n", entry.getKey()));
                for(String line : entry.getValue()) {
                    writer.println(String.format("\t\t%s", line));
                }
            }

        }

    }
}
