package com.sblukcic.runners;

public interface ProgramRunner {

    void run(String[] args) throws Exception;
}
