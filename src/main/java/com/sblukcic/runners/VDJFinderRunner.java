package com.sblukcic.runners;

import com.sblukcic.aggregation.Aggregator;
import com.sblukcic.aggregation.ComboAggregation;
import com.sblukcic.aggregation.VariableSequence;
import com.sblukcic.aggregation.amino.Amino;
import com.sblukcic.cli.Mode;
import com.sblukcic.cli.Parser;
import com.sblukcic.cli.ProgramArgs;
import com.sblukcic.combinations.CombinationFinder;
import com.sblukcic.combinations.RegionCombination;
import com.sblukcic.combinations.threads.RegionTagTypeAwareComboFinderThreadFactory;
import com.sblukcic.filter.GenericFilter;
import com.sblukcic.fqStream.FullLengthFastqFinder;
import com.sblukcic.information.InfoCollector;
import com.sblukcic.regions.REGION_TAG_TYPE;
import com.sblukcic.serialization.Serializer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@RequiredArgsConstructor
@Slf4j
public class VDJFinderRunner implements ProgramRunner {

    private final Parser parser;
    private final CombinationFinder finder;
    private final GenericFilter<List<ComboAggregation>> umiFilter;
    private final Aggregator aggregator;
    private final RegionTagTypeAwareComboFinderThreadFactory threadFactory;
    private final InfoCollector infoCollector;
    private final Serializer serializer;
    private final FullLengthFastqFinder fullLengthFastqFinder;

    @Override
    public void run(String[] in) throws Exception {

        log.info("Parsing input arguments");
        ProgramArgs args = parser.parse(in);

        ensureFolderExists(args);

        Path infoLocation = Paths.get(args.getDirectory() +
                File.separator +
                args.getInputFilePath()
                        .getFileName()
                        .toString()
                        .replaceAll(".fastq", "") + ".info");

        ensureFileExists(infoLocation);
        writeHeader(new FileOutputStream(infoLocation.toFile()));
        log.info("Collecting info on input params");
        collectArgInfo(args, in);

        log.info("Finding combinations");
        List<RegionCombination> combinations = getRegionCombinations(args);

        log.info("Aggregating found combinations");
        List<ComboAggregation> aggs = aggregator.aggregate(combinations);

        log.info("Filtering for UMIs");
        if(args.isCheckUMIs()){
            aggs = umiFilter.filter(aggs, args.getAllowedUMIMismatches());
        }


        int postFilterReads = 0;
        int productiveAminosAcids = 0;
        for(ComboAggregation agg : aggs){
            postFilterReads += countIds(agg.getVariableSequences());
            productiveAminosAcids += countProductiveAminos(agg.getVariableSequences());
        }

        infoCollector.addInfo("Filter stats",
                "Umi duplicates removed: " + (combinations.size() - postFilterReads),
                "Matched reads remaining: " + postFilterReads);

        infoCollector.addInfo("Combinations",
                "Productive combinations: " + productiveAminosAcids
                );

        log.info("Colling umi-filtered ids");
        List<String> ids = new ArrayList<>();
        for(ComboAggregation agg : aggs){
            for(VariableSequence seq : agg.getVariableSequences()){
                for(String id : seq.getIds()) {
                    ids.add(id.startsWith("@") ? id : '@' + id);
                }
            }
        }

        log.info("Collecting full length fastq sequences");
        File outputFastqFile = new File(args.getDirectory() +
                File.separator +
                args.getInputFilePath().getFileName().toString().replace(".fastq", "") +
                ".matches.fastq");
        ensureFileExists(Paths.get(outputFastqFile.toURI()));

        fullLengthFastqFinder.findFullLengthFastqs(ids,
                new FileInputStream(args.getInputFilePath().toFile()),
                new FileOutputStream(outputFastqFile));

        collectAggregatedInfo(aggs);

        log.info("Outputting info");
        infoCollector.write(new FileOutputStream(infoLocation.toFile(), true));

        log.info("Serializing to JSON format");
        Path jsonLocation = Paths.get(args.getDirectory() +
                File.separator +
                args.getInputFilePath()
                        .getFileName()
                        .toString()
                        .replaceAll(".fastq", "") + ".json");

        HashMap<String, Integer> map = new LinkedHashMap<>();
        map.put("total", combinations.size());
        map.put("unique", postFilterReads);
        map.put("duplicates", combinations.size() - postFilterReads);
        map.put("unique-productive", productiveAminosAcids);


        serializer.serialize(aggs, map, new FileOutputStream(jsonLocation.toFile()));

    }

    private List<RegionCombination> getRegionCombinations(ProgramArgs args) throws IOException {

        log.info("Finding V-J Combinations");
        if(args.getMode() == Mode.CODING){
            threadFactory.setCurrentType(REGION_TAG_TYPE.V_CODING, REGION_TAG_TYPE.J_CODING);
        }else{
            threadFactory.setCurrentType(REGION_TAG_TYPE.V_SIGNAL, REGION_TAG_TYPE.J_SIGNAL);
        }

        List<RegionCombination> combinations = new ArrayList<>();
        List<String> exclusions = new ArrayList<>();

        List<RegionCombination> vjCombinations = finder.find(args.getInputFilePath(), new ArrayList<>());
        for(RegionCombination combo : vjCombinations){
            combinations.add(combo);
            exclusions.add(combo.getId());
        }

        log.info("Finding V-V combinations");
        if(args.getMode() == Mode.CODING){
            threadFactory.setCurrentType(REGION_TAG_TYPE.V_CODING, REGION_TAG_TYPE.V_CODING);
        }else{
            threadFactory.setCurrentType(REGION_TAG_TYPE.V_SIGNAL, REGION_TAG_TYPE.V_SIGNAL);
        }

        List<RegionCombination> vvCombinations = finder.find(args.getInputFilePath(), exclusions);
        for(RegionCombination combo : vvCombinations){
            combinations.add(combo);
            exclusions.add(combo.getId());
        }

        log.info("Finding J-J combinations");
        if(args.getMode() == Mode.CODING){
            threadFactory.setCurrentType(REGION_TAG_TYPE.J_CODING, REGION_TAG_TYPE.J_CODING);
        }else{
            threadFactory.setCurrentType(REGION_TAG_TYPE.J_SIGNAL, REGION_TAG_TYPE.J_SIGNAL);
        }

        List<RegionCombination> jjCombinations = finder.find(args.getInputFilePath(), exclusions);
        combinations.addAll(jjCombinations);

        return combinations;
    }

    private void collectAggregatedInfo(List<ComboAggregation> aggs) {
        int alphaCombos = 0,
            alphaIds = 0,
            betaCombos = 0,
            betaIds = 0,
            gammaCombos = 0,
            gammaIds = 0,
            alphaVS = 0,
            betaVS = 0,
            gammaVS = 0;

        for(ComboAggregation agg : aggs){
            switch (agg.getOne().getLocus()){
                case GAMMA:
                    gammaCombos++;
                    gammaVS += agg.getVariableSequences().size();
                    gammaIds += countIds(agg.getVariableSequences());
                    break;
                case BETA:
                    betaCombos++;
                    betaVS += agg.getVariableSequences().size();
                    betaIds += countIds(agg.getVariableSequences());
                    break;
                default:
                    alphaCombos++;
                    alphaVS += agg.getVariableSequences().size();
                    alphaIds += countIds(agg.getVariableSequences());
            }
        }

        writeLocusInfo("Alphas", alphaCombos, alphaIds, alphaVS);
        writeLocusInfo("Betas", betaCombos, betaIds, betaVS);
        writeLocusInfo("Gammas", gammaCombos, gammaIds, gammaVS);

    }

    private void writeLocusInfo(String title, int combos, int ids, int vs){
        infoCollector.addInfo(title,
                "Number of combinations: " + combos,
                "Number of different variable sequences: " + vs,
                "Total number of reads found: "+ ids);
    }

    private int countProductiveAminos(List<VariableSequence> sequences){
        int count = 0;
        outer: for(VariableSequence s : sequences){
            for(Amino amino : s.getAminos()){
                if(amino.isProductive()) {
                    count++;
                    continue outer;
                }
            }
        }
        return count;
    }

    private int countIds(List<VariableSequence> sequences){
        int count = 0;
        for(VariableSequence s : sequences){
            count += s.getIds().size();
        }
        return count;
    }

    private void ensureFolderExists(ProgramArgs args) throws Exception {
        if(!args.getDirectory().toFile().exists()){
            if(!args.getDirectory().toFile().mkdirs()){
                throw new Exception("Cannot build output directory: " + args.getDirectory());
            }
        }
    }

    private void ensureFileExists(Path infoLocation) throws Exception {
        if(!infoLocation.toFile().exists()){
            if(!infoLocation.toFile().createNewFile()){
                throw new Exception("Cannot build info file: " + infoLocation.toString());
            }
        }
    }

    private void collectArgInfo(ProgramArgs args, String[] in) {

        StringJoiner spaceJoiner = new StringJoiner(" ");
        for(String s : in){
            spaceJoiner.add(s);
        }
        infoCollector.addInfo("Command-line", spaceJoiner.toString());
        infoCollector.addInfo("Parsed Arguments", in);
        infoCollector.addInfo("Program Arguments",
                "Input File: " + args.getInputFilePath().toString(),
                "Output Directory: " + args.getDirectory().toString(),
                "Mode: " + args.getMode().toString(),
                "Allowed UMI Mismatches: " + args.getAllowedUMIMismatches()
        );

    }

    private void writeHeader(OutputStream os){

        try(PrintWriter writer = new PrintWriter(new OutputStreamWriter(os))){
            final String HEADER_FORMAT = "%s %s";
            final DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss");
            writer.println(String.format(HEADER_FORMAT, "@HD", "VN:1.34 VDJGO"));
            writer.println(String.format(HEADER_FORMAT, "@PR", dtFormatter.format(LocalDateTime.now())));
            writer.println(String.format(HEADER_FORMAT, "@PR", "End"));
            writer.println("-----------------------------");
        }

    }
}
