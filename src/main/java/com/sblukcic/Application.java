package com.sblukcic;

import com.sblukcic.config.BeanConfig;
import com.sblukcic.runners.ProgramRunner;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@Slf4j(topic = "App")
public class Application {

    private static AnnotationConfigApplicationContext ctx;

    private static void init(){
        ctx = new AnnotationConfigApplicationContext();
        ctx.register(BeanConfig.class);
        ctx.refresh();
    }

    public static void main(String[] args) {

        try{
            init();
            ctx.getBean(ProgramRunner.class).run(args);

        }catch(Exception e){
            log.error(e.getMessage());
        }

    }
}
