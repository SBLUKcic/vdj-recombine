package com.sblukcic.aggregation;

import com.sblukcic.aggregation.amino.Amino;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.biojava.nbio.core.sequence.DNASequence;

import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Getter
public class VariableSequence {

    private final DNASequence sequence;
    private final List<Amino> aminos;
    private final List<String> ids = new LinkedList<>();

    public void addId(String id){
        this.ids.add(id);
    }
}
