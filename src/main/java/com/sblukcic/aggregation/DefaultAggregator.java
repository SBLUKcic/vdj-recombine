package com.sblukcic.aggregation;

import com.sblukcic.aggregation.amino.AminoTranslator;
import com.sblukcic.combinations.RegionCombination;
import com.sblukcic.regions.RegionTag;
import lombok.AllArgsConstructor;
import org.biojava.nbio.core.sequence.DNASequence;

import java.util.*;

@AllArgsConstructor
public class DefaultAggregator implements Aggregator {

    private final AminoTranslator translator;

    @Override
    public List<ComboAggregation> aggregate(List<RegionCombination> combinations) {

        Map<RegionPairKey, ComboAggregation> aggregateMap = new HashMap<>();

        outer: for(RegionCombination combination : combinations){
            RegionPairKey key = new RegionPairKey(combination.getOne(), combination.getTwo());
            if(!aggregateMap.containsKey(key)){
                ComboAggregation agg = new ComboAggregation(combination.getOne(), combination.getTwo());
                VariableSequence sequence = new VariableSequence(combination.getSequence(), translator.translate(
                        combination.getSequence(),
                        combination.getOne().getSequence(),
                        combination.getTwo().getSequence()
                        ));
                sequence.addId(combination.getId());
                agg.addVariableSequence(sequence);
                aggregateMap.put(key, agg);
                continue;
            }

            ComboAggregation agg = aggregateMap.get(key);
            for(VariableSequence vs : agg.getVariableSequences()){
                if(compareSequencesFR(vs.getSequence(), combination.getSequence())){
                    vs.addId(combination.getId());
                    continue outer;
                }
            }

            VariableSequence sequence = new VariableSequence(combination.getSequence(), translator.translate(
                    combination.getSequence(),
                    combination.getOne().getSequence(),
                    combination.getTwo().getSequence()
            ));
            sequence.addId(combination.getId());
            agg.addVariableSequence(sequence);
        }

        return new ArrayList<>(aggregateMap.values());
    }

    private boolean compareSequencesFR(DNASequence one, DNASequence two){
        if(one.getSequenceAsString().equals(two.getSequenceAsString())){
            return true;
        }

        return one.getReverseComplement().getSequenceAsString().equals(two.getSequenceAsString());
    }

    @AllArgsConstructor
    private static class RegionPairKey {

        private RegionTag one;
        private RegionTag two;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            RegionPairKey that = (RegionPairKey) o;
            return Objects.equals(one, that.one) &&
                    Objects.equals(two, that.two);
        }

        @Override
        public int hashCode() {
            return Objects.hash(one, two);
        }
    }
}
