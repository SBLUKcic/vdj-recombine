package com.sblukcic.aggregation;

import com.sblukcic.regions.RegionTag;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Getter
public class ComboAggregation {

    private final RegionTag one;
    private final RegionTag two;

    private final List<VariableSequence> variableSequences = new LinkedList<>();

    public void addVariableSequence(VariableSequence sequence){
        variableSequences.add(sequence);
    }

}
