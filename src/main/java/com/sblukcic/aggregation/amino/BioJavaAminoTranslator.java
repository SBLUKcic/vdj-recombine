package com.sblukcic.aggregation.amino;

import lombok.extern.slf4j.Slf4j;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.compound.AmbiguityDNACompoundSet;
import org.biojava.nbio.core.sequence.compound.AmbiguityRNACompoundSet;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.template.Sequence;
import org.biojava.nbio.core.sequence.transcription.Frame;
import org.biojava.nbio.core.sequence.transcription.TranscriptionEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public class BioJavaAminoTranslator implements AminoTranslator {

    private TranscriptionEngine engine;
    private Frame[] forwardFrames;

    public BioJavaAminoTranslator() {
        AmbiguityDNACompoundSet dnaCompoundSet = AmbiguityDNACompoundSet.getDNACompoundSet();
        AmbiguityRNACompoundSet rnaCompoundSet = AmbiguityRNACompoundSet.getRNACompoundSet();
        engine = new TranscriptionEngine
                .Builder()
                .trimStop(false) //needed to conserve * as the stop codon.
                .stopAtStopCodons(false)
                .dnaCompounds(dnaCompoundSet)
                .rnaCompounds(rnaCompoundSet)
                .build();

        forwardFrames = Frame.getForwardFrames();

    }

    @Override
    public List<Amino> translate(DNASequence sequence, DNASequence threePrimeSequence, DNASequence fivePrimeSequence) {

        List<Amino> aminos = new ArrayList<>();

        try{

            DNASequence full = new DNASequence(threePrimeSequence.getSequenceAsString() + sequence + fivePrimeSequence.getSequenceAsString());

            Map<Frame, Sequence<AminoAcidCompound>> threePrimeAminos = engine.multipleFrameTranslation(threePrimeSequence, forwardFrames);
            Map<Frame, Sequence<AminoAcidCompound>> fivePrimeAminos = engine.multipleFrameTranslation(fivePrimeSequence, forwardFrames);
            Map<Frame, Sequence<AminoAcidCompound>> results = engine.multipleFrameTranslation(full, forwardFrames);

            for(Frame f : forwardFrames){

                Sequence<AminoAcidCompound> threePAmino = threePrimeAminos.get(f);
                Sequence<AminoAcidCompound> fivePAmino = fivePrimeAminos.get(f);
                Sequence<AminoAcidCompound> fullAmino = results.get(f);

                boolean isProd = fullAmino.getSequenceAsString().startsWith(threePAmino.getSequenceAsString()) &&
                             fullAmino.getSequenceAsString().endsWith(fivePAmino.getSequenceAsString()) &&
                            !fullAmino.getSequenceAsString().contains("*");

                aminos.add(new Amino(f, threePAmino, fullAmino, fivePAmino, isProd));
            }

        }catch (CompoundNotFoundException cnfe){
            log.error(cnfe.getMessage());
        }

        return aminos;
    }
}
