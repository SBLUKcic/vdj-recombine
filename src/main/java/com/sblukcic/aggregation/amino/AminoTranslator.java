package com.sblukcic.aggregation.amino;

import org.biojava.nbio.core.sequence.DNASequence;

import java.util.List;

public interface AminoTranslator {

    List<Amino> translate(DNASequence sequence, DNASequence first, DNASequence second);
}
