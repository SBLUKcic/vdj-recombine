package com.sblukcic.aggregation.amino;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.template.Sequence;
import org.biojava.nbio.core.sequence.transcription.Frame;

@Getter
@AllArgsConstructor
public class Amino {

    private final Frame frame;
    private Sequence<AminoAcidCompound> threePrimeSeq;
    private Sequence<AminoAcidCompound> sequence;
    private Sequence<AminoAcidCompound> fivePrimeSeq;
    private boolean isProductive;

}
