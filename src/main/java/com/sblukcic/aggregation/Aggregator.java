package com.sblukcic.aggregation;

import com.sblukcic.combinations.RegionCombination;

import java.util.List;

public interface Aggregator {

    List<ComboAggregation> aggregate(List<RegionCombination> combinations);
}
