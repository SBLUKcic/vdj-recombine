package com.sblukcic.readers;

import org.biojava.nbio.genome.io.fastq.*;

import java.io.*;
import java.net.URL;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ExclusionReader implements FastqReader {

    Set<String> exclusions;

    public ExclusionReader(List<String> exclusions) {
        this.exclusions = new HashSet<>(exclusions);
    }

    @Override
    public void parse(Readable readable, ParseListener listener) throws IOException {
        throw new IOException("Not Implemented");
    }

    @Override
    public void stream(Readable readable, StreamListener listener) throws IOException {
        throw new IOException("Not Implemented");
    }

    @Override
    public Iterable<Fastq> read(File file) throws IOException {
        return read(new FileInputStream(file));
    }

    @Override
    public Iterable<Fastq> read(URL url) throws IOException {
        return read(new FileInputStream(new File(url.getFile())));
    }

    @Override
    public Iterable<Fastq> read(InputStream inputStream) throws IOException {

        List<Fastq> list = new ArrayList<>();
        FastqBuilder builder = new FastqBuilder();

        try(BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))){

            String description;
            String sequence;
            String quality;
            while((description = reader.readLine()) != null){
                sequence = reader.readLine();
                reader.readLine();
                quality = reader.readLine();
                if(!exclusions.contains(description)){
                    list.add(builder
                        .withSequence(sequence)
                        .withDescription(description)
                        .withQuality(quality)
                        .withVariant(FastqVariant.FASTQ_SANGER)
                        .build());
                }
            }
        }
        return list;
    }
}
