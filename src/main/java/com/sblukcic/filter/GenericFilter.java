package com.sblukcic.filter;

public interface GenericFilter<T> {

    T filter(T t, int... settings);
}
