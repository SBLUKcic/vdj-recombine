package com.sblukcic.filter;

import java.util.Comparator;

public class StringDifferenceComparator implements Comparator<String> {
    @Override
    public int compare(String o1, String o2) {

        if(o1.length() != o2.length()){
            return -1;
        }

        int diffs = 0;
        for(int i = 0; i < o2.length(); i++){
            if(o1.charAt(i) != o2.charAt(i)){
                diffs++;
            }
        }
        return diffs;
    }
}
