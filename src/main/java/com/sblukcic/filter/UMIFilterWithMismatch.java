package com.sblukcic.filter;

import com.sblukcic.aggregation.ComboAggregation;
import com.sblukcic.aggregation.VariableSequence;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j(topic = "umi-filter")
@RequiredArgsConstructor
public class UMIFilterWithMismatch implements GenericFilter<List<ComboAggregation>> {

    private final Pattern bcr1Pattern = Pattern.compile("BCR1:(.*?):");
    private final Pattern bcr2Pattern = Pattern.compile("BCR2:(.*?)/");
    private final Pattern bcr2PatternEnd = Pattern.compile("BCR2:(.*?)$");
    private int allowedMismatches = 0;
    private final Comparator<String> differentComparator;


    @Override
    public List<ComboAggregation> filter(List<ComboAggregation> comboAggregations, int... settings) {

        if(settings != null && settings.length > 0){
            allowedMismatches = settings[0];
        }

        List<ComboAggregation> clearedAggs = new ArrayList<>(comboAggregations.size());

        for(ComboAggregation agg : comboAggregations){

            ComboAggregation newAgg = new ComboAggregation(agg.getOne(), agg.getTwo());

            //filter
            List<String> allIds = new ArrayList<>();
            for(VariableSequence sequence : agg.getVariableSequences()){
                allIds.addAll(sequence.getIds());
            }
            Set<String> filteredIds = new HashSet<>(filterForUMI(allIds));

            //reconstruct while checking filtered ids
            for(VariableSequence sequence : agg.getVariableSequences()){
                VariableSequence newSeq = new VariableSequence(sequence.getSequence(), sequence.getAminos());
                for(String id : sequence.getIds()){
                    if(filteredIds.contains(id)){
                        newSeq.addId(id);
                    }
                }
                if(newSeq.getIds().size() > 0){
                    newAgg.addVariableSequence(newSeq);
                }

            }

            clearedAggs.add(newAgg);
        }

        return clearedAggs;
    }

    private List<String> filterForUMI(List<String> ids){

        List<String> filteredIds = new ArrayList<>(ids.size());
        Set<String> foundUMIs = new HashSet<>();
        outer: for(String id : ids){
            String bcr1 = findWithMatcher(bcr1Pattern, id);
            String bcr2 = findMultiPatterns(id, bcr2Pattern, bcr2PatternEnd);

            if(bcr1 == null || bcr2 == null){
                //can't filter on UMI if there isn't a UMI.
                filteredIds.add(id);
                continue;
            }

            try {

                DNASequence bcr1Seq = new DNASequence(bcr1);
                DNASequence bcr2Seq = new DNASequence(bcr2);

                String umi =
                     Stream.of(bcr1Seq.getSequenceAsString(),
                         bcr2Seq.getSequenceAsString())
                     .sorted(String::compareTo)
                     .collect(Collectors.joining());

                if(!foundUMIs.contains(umi)){
                    //first one with this particular umi.
                    if(allowedMismatches > 0){

                        for(String found : foundUMIs){
                            int diff = differentComparator.compare(found, umi);
                            if(diff <= allowedMismatches){
                                continue outer;
                            }
                        }

                    }

                    filteredIds.add(id);
                    foundUMIs.add(umi);
                    foundUMIs.add(bcr1Seq.getReverseComplement().getSequenceAsString() + bcr2Seq.getReverseComplement().getSequenceAsString());

                }

            } catch (CompoundNotFoundException e) {
                log.error("Cannot create umi sequence: " + e.getMessage());
                log.error("This may affect the UMI filter");
            }

        }

        return filteredIds;
    }

    private String findMultiPatterns(String data, Pattern... patterns){
        for(Pattern p : patterns){
            String ret = findWithMatcher(p, data);
            if(ret != null){
                return ret;
            }
        }

        return null;
    }

    //todo - this is also in the FastaRegionReader - pull out into utils
    private String findWithMatcher(Pattern pattern, String data){
        Matcher matcher = pattern.matcher(data);

        if(matcher.find()){
            return matcher.group(1);
        }

        return null;
    }

}
