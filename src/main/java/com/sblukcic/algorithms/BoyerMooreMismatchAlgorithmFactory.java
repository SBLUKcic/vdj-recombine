package com.sblukcic.algorithms;

public class BoyerMooreMismatchAlgorithmFactory implements MismatchAwareAlgorithmFactory {

    private int mismatches = 0;
    @Override
    public SearchAlgorithm createSearchAlgorithm(String pattern) {
        return new BoyerMooreMismatchAlgorithm(pattern, mismatches);
    }

    @Override
    public void setAllowedMismatches(int mismatches) {
        this.mismatches = mismatches;
    }

    @Override
    public int getAllowedMismatches() {
        return mismatches;
    }
}
