package com.sblukcic.algorithms;

public interface MismatchAwareAlgorithmFactory extends MismatchAware, SearchAlgorithmFactory {
}
