package com.sblukcic.algorithms;

public interface MismatchAware {

    void setAllowedMismatches(int mismatches);

    int getAllowedMismatches();
}
