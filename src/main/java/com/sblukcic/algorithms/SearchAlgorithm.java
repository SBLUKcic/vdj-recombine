package com.sblukcic.algorithms;

/**
 * Implementations should create a string search algorithm
 */

public interface SearchAlgorithm {

    /** search the text for the pattern
     * @param pattern text to search
     * @return position of the first character of the patter found in the text, -1 if not found
     */
    int search(String pattern);
}
