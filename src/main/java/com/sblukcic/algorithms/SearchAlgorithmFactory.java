package com.sblukcic.algorithms;

public interface SearchAlgorithmFactory {

    SearchAlgorithm createSearchAlgorithm(String pattern);
}
