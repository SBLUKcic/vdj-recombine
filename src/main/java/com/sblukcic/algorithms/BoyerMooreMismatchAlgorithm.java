package com.sblukcic.algorithms;

public class BoyerMooreMismatchAlgorithm implements SearchAlgorithm {

    private final char[] pattern; //pattern to index
    private final int mismatches;

    public BoyerMooreMismatchAlgorithm(String pattern, int mismatches) {
        this.pattern = pattern.toCharArray();
        this.mismatches = mismatches;
    }

    @Override
    public int search(String text) {
        int m = pattern.length;
        char[] textArr = text.toCharArray();
        int n = textArr.length;
        if(n < m) return -1; //pattern cannot be found if the text is less than the pattern

        int skip;
        for (int i = 0; i <= n - m; i += skip) {
            skip = 0;
            int count = 0;

            for (int j = m-1; j >= 0; j--) {
                if (pattern[j] != textArr[i+j] && count >= mismatches) {
                    skip = 1;
                    break;
                }
                if(pattern[j] != textArr[i+j]){
                    count ++;
                }
            }
            if (skip == 0) return i;    // found
        }
        return -1;
    }
}
