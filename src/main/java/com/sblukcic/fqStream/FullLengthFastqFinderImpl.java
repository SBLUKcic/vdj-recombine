package com.sblukcic.fqStream;

import java.io.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FullLengthFastqFinderImpl implements FullLengthFastqFinder {
    @Override
    public void findFullLengthFastqs(List<String> ids, InputStream stream, OutputStream outputStream) throws IOException {

        Set<String> hashSet = new HashSet<>(ids);

        try(BufferedReader reader = new BufferedReader(new InputStreamReader(stream))){

            try(PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream))){

                String id;
                String seq;
                String phred;
                while((id = reader.readLine()) != null){

                    seq = reader.readLine();
                    reader.readLine();
                    phred = reader.readLine();

                    if(hashSet.contains(id)){
                        writer.println(id);
                        writer.println(seq);
                        writer.println('+');
                        writer.println(phred);
                    }

                }

            }

        }

    }
}
