package com.sblukcic.fqStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public interface FullLengthFastqFinder {

    void findFullLengthFastqs(List<String> ids, InputStream stream, OutputStream outputStream) throws IOException;
}
