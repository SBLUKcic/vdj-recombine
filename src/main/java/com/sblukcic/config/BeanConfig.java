package com.sblukcic.config;

import com.sblukcic.aggregation.Aggregator;
import com.sblukcic.aggregation.ComboAggregation;
import com.sblukcic.aggregation.DefaultAggregator;
import com.sblukcic.aggregation.amino.AminoTranslator;
import com.sblukcic.aggregation.amino.BioJavaAminoTranslator;
import com.sblukcic.algorithms.BoyerMooreMismatchAlgorithmFactory;
import com.sblukcic.algorithms.MismatchAwareAlgorithmFactory;
import com.sblukcic.cli.JCommanderParser;
import com.sblukcic.cli.Parser;
import com.sblukcic.combinations.CombinationFinder;
import com.sblukcic.combinations.ConcurrentCombinationFinder;
import com.sblukcic.combinations.DefaultRegionCombinationFactory;
import com.sblukcic.combinations.RegionCombinationFactory;
import com.sblukcic.combinations.threads.DefaultCombinationFinderThreadFactory;
import com.sblukcic.combinations.threads.RegionTagTypeAwareComboFinderThreadFactory;
import com.sblukcic.filter.GenericFilter;
import com.sblukcic.filter.StringDifferenceComparator;
import com.sblukcic.filter.UMIFilterWithMismatch;
import com.sblukcic.fqStream.FullLengthFastqFinder;
import com.sblukcic.fqStream.FullLengthFastqFinderImpl;
import com.sblukcic.information.AppendingInfoCollector;
import com.sblukcic.information.InfoCollector;
import com.sblukcic.regions.*;
import com.sblukcic.runners.ProgramRunner;
import com.sblukcic.runners.VDJFinderRunner;
import com.sblukcic.serialization.JsonSerializer;
import com.sblukcic.serialization.Serializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class BeanConfig {

    @Bean
    public RegionProvider regionProvider() throws IOException {
        RegionTagFactory regionTagFactory = new TRRegionTagFactory();
        RegionReader regionReader = new FastaRegionReader(regionTagFactory);
        ResourceLoader rl = new DefaultResourceLoader();
        Map<REGION_TAG_TYPE, InputStream> locationMap = new HashMap<REGION_TAG_TYPE, InputStream>(){{
            put(REGION_TAG_TYPE.V_CODING, rl.getResource("classpath:v-region-defaults_coding.fasta").getInputStream());
            put(REGION_TAG_TYPE.V_CODING_ALT, rl.getResource("classpath:v-region-defaults_coding_alt.fasta").getInputStream());
            put(REGION_TAG_TYPE.V_SIGNAL, rl.getResource("classpath:v-region-defaults_signal.fasta").getInputStream());
            put(REGION_TAG_TYPE.V_SIGNAL_ALT, rl.getResource("classpath:v-region-defaults_signal_alt.fasta").getInputStream());
            put(REGION_TAG_TYPE.J_SIGNAL, rl.getResource("classpath:j-region-defaults_signal.fasta").getInputStream());
            put(REGION_TAG_TYPE.J_CODING, rl.getResource("classpath:j-region-defaults_coding.fasta").getInputStream());
            put(REGION_TAG_TYPE.J_CODING_ALT, rl.getResource("classpath:j-region-defaults_coding_alt.fasta").getInputStream());
        }};

        return new CachingFastaRegionProvider(locationMap, regionReader);
    }

    @Bean(name = "umi-filter")
    public GenericFilter<List<ComboAggregation>> filter(){
        return new UMIFilterWithMismatch(new StringDifferenceComparator());
    }

    @Bean
    public FullLengthFastqFinder fullLengthFastqFinder(){
        return new FullLengthFastqFinderImpl();
    }

    @Bean
    public InfoCollector infoCollector() {
        return new AppendingInfoCollector();
    }

    @Bean
    public AminoTranslator aminoTranslator(){
        return new BioJavaAminoTranslator();
    }

    @Bean
    public Aggregator aggregator(AminoTranslator aminoTranslator){
        return new DefaultAggregator(aminoTranslator);
    }

    @Bean
    public Serializer serializer(){
        return new JsonSerializer();
    }

    @Bean
    public MismatchAwareAlgorithmFactory algorithmFactory(){
        return new BoyerMooreMismatchAlgorithmFactory();
    }

    @Bean
    public RegionCombinationFactory regionCombinationFactory(){
        return new DefaultRegionCombinationFactory();
    }

    @Bean
    @Autowired
    public RegionTagTypeAwareComboFinderThreadFactory threadFactory(RegionProvider regionProvider,
                                                                    MismatchAwareAlgorithmFactory algorithmFactory,
                                                                    RegionCombinationFactory regionCombinationFactory){
        return new DefaultCombinationFinderThreadFactory(regionProvider,
                algorithmFactory,
                regionCombinationFactory);
    }

    @Bean
    @Autowired
    public CombinationFinder combinationFinder(InfoCollector collector,
            RegionTagTypeAwareComboFinderThreadFactory threadFactory){
        int cores = Runtime.getRuntime().availableProcessors();
        int fastqPerCore = 200;
        return new ConcurrentCombinationFinder(threadFactory,
                collector,
                fastqPerCore,
                cores);
    }


    @Bean
    public Parser parser() {
        return new JCommanderParser();
    }

    @Bean
    @Autowired
    public ProgramRunner runner(@Qualifier("umi-filter") GenericFilter<List<ComboAggregation>> umiFilter,
                                Parser parser,
                                CombinationFinder finder,
                                Aggregator aggregator,
                                InfoCollector infoCollector,
                                RegionTagTypeAwareComboFinderThreadFactory threadFactory,
                                Serializer serializer,
                                FullLengthFastqFinder fullLengthFastqFinder) {
        return new VDJFinderRunner(parser, finder, umiFilter, aggregator, threadFactory, infoCollector, serializer, fullLengthFastqFinder);
    }


}
