package com.sblukcic.regions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.biojava.nbio.core.sequence.DNASequence;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
@Getter
public class RegionTag {

    private final String name;
    private final DNASequence sequence;
    private final Locus locus;
    private final DNASequence rssBases;
    private final boolean isRepetitive;

    private List<RegionTag> alternativeTags = new ArrayList<>();

    public void addAlternativeTag(RegionTag tag){
        alternativeTags.add(tag);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegionTag regionTag = (RegionTag) o;
        return Objects.equals(name, regionTag.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
