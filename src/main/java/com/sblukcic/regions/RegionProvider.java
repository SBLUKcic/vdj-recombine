package com.sblukcic.regions;

import java.util.List;
import java.util.Map;

public interface RegionProvider {

    List<RegionTag> getRegionsForTagType(REGION_TAG_TYPE type) throws Exception;

    Map<REGION_TAG_TYPE, List<RegionTag>> getRegions() throws Exception;
}
