package com.sblukcic.regions;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j(topic = "Fasta Region Reader")
@RequiredArgsConstructor
public class FastaRegionReader implements RegionReader {

    private final Pattern namePattern = Pattern.compile(">(.*?):RSS");
    private final Pattern rssPattern = Pattern.compile("RSS=(.*?)$");
    private final Pattern altNamePattern = Pattern.compile(">(.*?)$");
    private final RegionTagFactory regionTagFactory;

    @Override
    public List<RegionTag> findRegionTags(InputStream mainStream, InputStream altStream) throws Exception  {

        ByteArrayOutputStream altCopy = new ByteArrayOutputStream();

        if(altStream != null){
            IOUtils.copy(altStream, altCopy);
        }


        List<RegionTag> tags = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(mainStream))){

            String id;
            while((id = reader.readLine()) != null){
                if(id.startsWith("#")) continue;
                String sequence = reader.readLine();
                if(sequence == null) throw new IOException("Invalid number of lines");

                String name = findWithMatcher(namePattern, id);
                String rss = findWithMatcher(rssPattern, id);

                RegionTag tag = regionTagFactory.createRegionTag(name, sequence, rss);

                if(tag.isRepetitive()){
                    findAlternativeTags(tag, new ByteArrayInputStream(altCopy.toByteArray()));
                }

                tags.add(tag);

            }

        }

        return tags;
    }

    private void findAlternativeTags(RegionTag tag, InputStream altStream) throws Exception{

        try(BufferedReader reader = new BufferedReader(new InputStreamReader(altStream))){

            String id;
            while((id = reader.readLine()) != null){
                if(id.startsWith("#")) continue;
                String sequence = reader.readLine();
                if(sequence == null){
                    throw new IOException("Invalid number of lines");
                }

                String name = findWithMatcher(altNamePattern, id);

                if(tag.getName().contains(name.replaceAll("_alt", ""))){
                    RegionTag alt = regionTagFactory.createRegionTag(name, sequence, "");
                    tag.addAlternativeTag(alt);
                }

            }

        }

    }

    private String findWithMatcher(Pattern pattern, String data){
        Matcher matcher = pattern.matcher(data);

        if(matcher.find()){
            return matcher.group(1);
        }

        return data;
    }
}
