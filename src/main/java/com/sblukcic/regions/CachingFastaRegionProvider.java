package com.sblukcic.regions;

import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class CachingFastaRegionProvider implements RegionProvider {

    private Map<REGION_TAG_TYPE, InputStream> locationMap;
    private Map<REGION_TAG_TYPE, List<RegionTag>> regionTagMap;
    private final RegionReader regionReader;

    public CachingFastaRegionProvider(Map<REGION_TAG_TYPE, InputStream> locationMap, RegionReader reader) {
       this.locationMap = locationMap;
       this.regionTagMap = new HashMap<>();
       this.regionReader = reader;
    }

    @Override
    public List<RegionTag> getRegionsForTagType(REGION_TAG_TYPE type) throws  Exception {
        if(!regionTagMap.containsKey(type)){
            regionTagMap.put(type, regionReader.findRegionTags(locationMap.get(type),
                       locationMap.get(REGION_TAG_TYPE.getAlternative(type))));
        }
        return regionTagMap.get(type);
    }

    @Override
    public Map<REGION_TAG_TYPE, List<RegionTag>> getRegions() throws  Exception {
        for(val entry : locationMap.entrySet()){
            if(!regionTagMap.containsKey(entry.getKey())){
                if(!REGION_TAG_TYPE.isAltList(entry.getKey())) {
                    regionTagMap.put(entry.getKey(), regionReader
                            .findRegionTags(entry.getValue(),
                                    locationMap.get(REGION_TAG_TYPE.getAlternative(entry.getKey()))
                            )
                    );
                }
            }
        }
        return regionTagMap;
    }


}
