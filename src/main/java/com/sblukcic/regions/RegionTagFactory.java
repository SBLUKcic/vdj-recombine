package com.sblukcic.regions;

import org.biojava.nbio.core.exceptions.CompoundNotFoundException;

public interface RegionTagFactory {

    RegionTag createRegionTag(String name, String sequence, String rssBases) throws CompoundNotFoundException;
}
