package com.sblukcic.regions;

public enum REGION_TAG_TYPE {
    J_CODING,
    J_CODING_ALT,
    J_SIGNAL,
    J_SIGNAL_ALT,
    V_CODING,
    V_CODING_ALT,
    V_SIGNAL,
    V_SIGNAL_ALT;

    public static boolean isAltList(REGION_TAG_TYPE type){
        switch (type){
            case J_CODING_ALT:
            case V_CODING_ALT:
            case J_SIGNAL_ALT:
            case V_SIGNAL_ALT:
                return true;
        }

        return false;
    }

    public static REGION_TAG_TYPE getAlternative(REGION_TAG_TYPE type){
        switch (type){
            case J_CODING:
                return J_CODING_ALT;
            case J_SIGNAL:
                return J_SIGNAL_ALT;
            case V_CODING:
                return V_CODING_ALT;
            case V_SIGNAL:
                return V_SIGNAL_ALT;
            default: return type;
        }
    }
}
