package com.sblukcic.regions;

public enum Locus {

    ALPHA,
    BETA,
    GAMMA
}
