package com.sblukcic.regions;

import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TRRegionTagFactory implements RegionTagFactory {

    private final static String ALPHA_START = "TRA";
    private final static String BETA_START = "TRB";
    private final static String GAMMA_START = "TRG";
    private final static String DELTA_START = "TRD";
    private final static String R_TAG_START = "rtag";
    private final Pattern namePattern = Pattern.compile("\\((.*?)\\)");

    @Override
    public RegionTag createRegionTag(String name, String sequence, String rssBases) throws CompoundNotFoundException {

        boolean isRepetitive = name.toLowerCase().startsWith(R_TAG_START);

        String nameCheck = isRepetitive ? findRepNames(name).toUpperCase() : name.toUpperCase();

        Locus locus = nameCheck.startsWith(ALPHA_START) || nameCheck.startsWith(DELTA_START) ? Locus.ALPHA :
                nameCheck.startsWith(BETA_START) ? Locus.BETA : Locus.GAMMA;

        return new RegionTag(
                name,
                new DNASequence(sequence),
                locus,
                new DNASequence(rssBases),
                isRepetitive
        );
    }

    private String findRepNames(String name){
        Matcher matcher = namePattern.matcher(name);

        if(matcher.find()){
            return matcher.group(1);
        }

        return name;
    }

}
