package com.sblukcic.regions;

import java.io.InputStream;
import java.util.List;

public interface RegionReader {

    List<RegionTag> findRegionTags(InputStream mainStream, InputStream altStream) throws Exception;

}
