package com.sblukcic.serialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;
import com.sblukcic.aggregation.ComboAggregation;
import com.sblukcic.aggregation.VariableSequence;
import com.sblukcic.aggregation.amino.Amino;
import com.sblukcic.combinations.RegionCombination;
import com.sblukcic.regions.RegionTag;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Map;

public class JsonSerializer implements Serializer {

    @Override
    public void serialize(List<ComboAggregation> aggregations, Map<String, Integer> defaultStats, OutputStream stream) throws IOException {

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        try(JsonWriter writer = gson.newJsonWriter((new OutputStreamWriter(stream)))){
            writer.beginObject();
            defaultStats.forEach((key, val) -> {
                try {
                    writer.name(key).value(val);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.name("combinations").beginArray();
            for(ComboAggregation aggregation : aggregations){
                writer.beginObject();
                writer.name("regions").beginArray();
                serializeRegionTag(writer, aggregation.getOne(), "3p");
                serializeRegionTag(writer, aggregation.getTwo(), "5p");
                writer.endArray();
                writer.name("variableSequences").beginArray();
                for(VariableSequence seq : aggregation.getVariableSequences()){
                    writer.beginObject();
                    writer.name("sequence").value(seq.getSequence().getSequenceAsString());
                    writer.name("aminos").beginArray();
                    for(Amino a : seq.getAminos()){
                        writer.beginObject();
                        writer.name("frame").value(a.getFrame().name());
                        writer.name("threePrimeTag").value(a.getThreePrimeSeq().getSequenceAsString());
                        writer.name("fivePrimeTag").value(a.getFivePrimeSeq().getSequenceAsString());
                        writer.name("sequence").value(a.getSequence().getSequenceAsString());
                        writer.name("isProductive").value(a.isProductive());
                        writer.endObject();
                    }
                    writer.endArray();
                    writer.name("ids").beginArray();
                    for(String id : seq.getIds()){
                        writer.value(id);
                    }
                    writer.endArray();
                    writer.endObject();
                }
                writer.endArray();
                writer.endObject();
            }
            writer.endArray();
            writer.endObject();
        }

    }

    private void serializeRegionTag(JsonWriter writer, RegionTag tag, String end) throws IOException {

        writer.beginObject();
        writer.name("name").value(tag.getName());
        writer.name("position").value(end);
        writer.name("locus").value(tag.getLocus().name());
        writer.name("seqToRSS").value(tag.getRssBases().getSequenceAsString());
        writer.name("seq").value(tag.getSequence().getSequenceAsString());
        writer.endObject();

    }
}
