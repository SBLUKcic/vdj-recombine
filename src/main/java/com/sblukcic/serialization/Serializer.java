package com.sblukcic.serialization;

import com.sblukcic.aggregation.ComboAggregation;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

public interface Serializer {

    void serialize(List<ComboAggregation> combinations, Map<String, Integer> defaultStats, OutputStream stream) throws IOException;
}
