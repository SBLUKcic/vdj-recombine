package com.sblukcic.combinations;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface CombinationFinder {

    List<RegionCombination> find(Path path, List<String> excludedIds) throws IOException;
}
