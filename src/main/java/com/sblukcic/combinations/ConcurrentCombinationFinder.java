package com.sblukcic.combinations;

import com.sblukcic.combinations.threads.CombinationFinderThreadFactory;
import com.sblukcic.information.InfoCollector;
import com.sblukcic.readers.ExclusionReader;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.biojava.nbio.genome.io.fastq.*;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

@AllArgsConstructor
@Slf4j
public class ConcurrentCombinationFinder implements CombinationFinder {

    private final CombinationFinderThreadFactory threadFactory;
    private final InfoCollector collector;
    private final int FASTQ_THRESHOLD;
    private final int CORES;


    @Override
    public List<RegionCombination> find(Path path, List<String> excludedIds) throws IOException {

        List<RegionCombination> allCombinations = new ArrayList<>();

        FastqReader reader = new ExclusionReader(excludedIds);

        int submitted = 0;

        ExecutorService WORKER_THREAD_POOL = Executors.newFixedThreadPool(CORES);
        CompletionService<List<RegionCombination>> service = new ExecutorCompletionService<>(WORKER_THREAD_POOL);
        InputStream is = Files.newInputStream(path);

        log.info("Reading input fastq file");
        int counted = 0;
        Iterable<Fastq> iterable = reader.read(is);

        log.info("Distributing records");
        List<Fastq> chunk = new ArrayList<>(FASTQ_THRESHOLD);
        for(Fastq fastq : iterable){

            chunk.add(fastq);
            counted++;
            if(chunk.size() >= FASTQ_THRESHOLD){
                submitted ++;
                submit(service, chunk);
                chunk = new ArrayList<>(FASTQ_THRESHOLD);
            }

        }

        if(chunk.size() > 0){
            submitted++;
            submit(service, chunk);
        }

        int received = 0;
        boolean errors = false;

        log.info("All FASTQ records submitted, waiting for " + (submitted - received) + " to return");

        while(received < submitted && !errors){
            try {
                Future<List<RegionCombination>> future = service.take();
                try{
                    List<RegionCombination> combos = future.get();
                    allCombinations.addAll(combos);
                    log.info(received + " / " + submitted);
                    received++;
                }catch (Exception e){
                    errors = true;
                }
            } catch (InterruptedException e) {
                log.error(e.getMessage());
            }
        }

        log.info("All combinations returned");
        WORKER_THREAD_POOL.shutdown();
        try {
            WORKER_THREAD_POOL.awaitTermination(2, TimeUnit.MICROSECONDS);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
        }

        if(allCombinations.size() > 0){
            collector.addInfo("Combinations",
                    "Input reads: " + counted,
                    "Matched reads: " + allCombinations.size(),
                    "Unmatched reads: " + (counted - allCombinations.size()));
        }


        return allCombinations;
    }

    private void submit(CompletionService<List<RegionCombination>> service, List<Fastq> fastqs) throws IOException{
        try{
            service.submit(threadFactory.create(fastqs));
        }catch (Exception e){
            throw new IOException(e.getMessage());
        }
    }


}
