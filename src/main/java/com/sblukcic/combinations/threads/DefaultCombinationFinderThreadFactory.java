package com.sblukcic.combinations.threads;

import com.sblukcic.algorithms.SearchAlgorithmFactory;
import com.sblukcic.combinations.RegionCombination;
import com.sblukcic.combinations.RegionCombinationFactory;
import com.sblukcic.regions.REGION_TAG_TYPE;
import com.sblukcic.regions.RegionProvider;
import lombok.RequiredArgsConstructor;
import org.biojava.nbio.genome.io.fastq.Fastq;

import java.util.List;
import java.util.concurrent.Callable;

@RequiredArgsConstructor
public class DefaultCombinationFinderThreadFactory implements RegionTagTypeAwareComboFinderThreadFactory {

    private final RegionProvider provider;
    private final SearchAlgorithmFactory algorithmFactory;
    private final RegionCombinationFactory regionCombinationFactory;
    private REGION_TAG_TYPE one = REGION_TAG_TYPE.V_SIGNAL;
    private REGION_TAG_TYPE two = REGION_TAG_TYPE.J_SIGNAL;

    @Override
    public Callable<List<RegionCombination>> create(List<Fastq> fastqs) throws Exception {
        return new CombinationFinderCallable(fastqs,
                provider.getRegionsForTagType(one),
                provider.getRegionsForTagType(two),
                algorithmFactory,
                regionCombinationFactory);
    }

    @Override
    public void setCurrentType(REGION_TAG_TYPE one, REGION_TAG_TYPE two) {
        this.one = one;
        this.two = two;
    }
}
