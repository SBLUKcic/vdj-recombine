package com.sblukcic.combinations.threads;

public interface RegionTagTypeAwareComboFinderThreadFactory extends RegionTagTypeComboAware, CombinationFinderThreadFactory {
}
