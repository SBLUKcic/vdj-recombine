package com.sblukcic.combinations.threads;

import com.sblukcic.regions.REGION_TAG_TYPE;

public interface RegionTagTypeComboAware {

    void setCurrentType(REGION_TAG_TYPE one, REGION_TAG_TYPE two);
}
