package com.sblukcic.combinations.threads;

import com.sblukcic.algorithms.SearchAlgorithm;
import com.sblukcic.algorithms.SearchAlgorithmFactory;
import com.sblukcic.combinations.RegionCombination;
import com.sblukcic.combinations.RegionCombinationFactory;
import com.sblukcic.regions.RegionTag;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.genome.io.fastq.Fastq;

import java.util.*;
import java.util.concurrent.Callable;

/** Will find region combinations for the input region tag lists + fastq reads (Will search both forward an reverse compliment). */
@RequiredArgsConstructor
public class CombinationFinderCallable implements Callable<List<RegionCombination>> {

    private final List<Fastq> fastqs;
    private final List<RegionTag> listOne;
    private final List<RegionTag> listTwo;
    private final SearchAlgorithmFactory searchAlgorithmFactory;
    private final RegionCombinationFactory regionCombinationFactory;

    @Override
    public List<RegionCombination> call() throws Exception {

        Map<RegionTag, SearchAlgorithm> listOneAlgorithmMap = createSearchAlgorithmMap(listOne);
        Map<RegionTag, SearchAlgorithm> listTwoAlgorithmMap = createSearchAlgorithmMap(listTwo);
        Map<RegionTag, SearchAlgorithm> listOneAltAlgorithmMap = createAltSearchAlgorithmMap(listOne);
        Map<RegionTag, SearchAlgorithm> listTwoAltAlgorithmMap = createAltSearchAlgorithmMap(listTwo);
        List<RegionCombination> found = new ArrayList<>();

        outer: for(Fastq fastq : fastqs){
            List<String> sequences = Arrays.asList(
                    fastq.getSequence(),
                    new DNASequence(fastq.getSequence()).getReverseComplement().getSequenceAsString());

            for(String sequence : sequences){
                SearchResult result1 = findMatch(listOneAlgorithmMap, listOneAltAlgorithmMap, sequence);
                SearchResult result2 = findMatch(listTwoAlgorithmMap, listTwoAltAlgorithmMap, sequence);

                if(result1 != null && result2 != null){
                    if(result1.getTag().getLocus() == result2.getTag().getLocus()){
                        int lowerIndex = Math.min(result1.getIndex(), result2.getIndex());
                        int higherIndex = Math.max(result1.getIndex(), result2.getIndex());

                        RegionTag lowerTag = lowerIndex == result1.getIndex() ? result1.getTag() : result2.getTag();
                        RegionTag highTag = lowerIndex == result1.getIndex() ? result2.getTag() : result1.getTag();

                        String sequenceBetweenTags = sequence.substring(lowerIndex + lowerTag.getSequence().getLength(), higherIndex);

                        RegionCombination combo = regionCombinationFactory.create(lowerTag, highTag, sequenceBetweenTags, fastq.getDescription());
                        found.add(combo);
                        continue outer;
                    }

                }
            }
        }

        return found;
    }

    private SearchResult findMatch(Map<RegionTag, SearchAlgorithm> map, Map<RegionTag, SearchAlgorithm> altMap, String sequence){

        for(val entry : map.entrySet()){
            int index = entry.getValue().search(sequence);
            if(index != -1){
                if(entry.getKey().isRepetitive()){
                    for(RegionTag rTag : entry.getKey().getAlternativeTags()){
                        SearchAlgorithm algorithm = altMap.get(rTag);
                        int rIndex = algorithm.search(sequence);
                        if(rIndex != -1){
                            return new SearchResult(entry.getKey(), index);
                        }
                    }
                }
                return new SearchResult(entry.getKey(), index);
            }
        }
        return null;
    }

    private Map<RegionTag, SearchAlgorithm> createSearchAlgorithmMap(List<RegionTag> tags){
        Map<RegionTag, SearchAlgorithm> map = new HashMap<>();
        for(RegionTag tag : tags){
            map.put(tag, searchAlgorithmFactory.createSearchAlgorithm(tag.getSequence().getSequenceAsString()));
        }
        return map;
    }

    private Map<RegionTag, SearchAlgorithm> createAltSearchAlgorithmMap(List<RegionTag> tags){
        Map<RegionTag, SearchAlgorithm> map = new HashMap<>();
        for(RegionTag tag : tags){
            if(tag.isRepetitive()){
                for(RegionTag rTag : tag.getAlternativeTags()){
                    map.put(rTag, searchAlgorithmFactory.createSearchAlgorithm(rTag.getSequence().getSequenceAsString()));
                }
            }
        }
        return map;
    }

    @AllArgsConstructor
    @Getter
    private static class SearchResult {
        private RegionTag tag;
        private int index;
    }
}
