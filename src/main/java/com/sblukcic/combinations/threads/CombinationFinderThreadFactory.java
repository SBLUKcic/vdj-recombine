package com.sblukcic.combinations.threads;

import com.sblukcic.combinations.RegionCombination;
import org.biojava.nbio.genome.io.fastq.Fastq;

import java.util.List;
import java.util.concurrent.Callable;

public interface CombinationFinderThreadFactory {

    Callable<List<RegionCombination>> create(List<Fastq> fastqs) throws Exception;
}
