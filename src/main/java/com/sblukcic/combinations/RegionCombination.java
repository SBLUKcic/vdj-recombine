package com.sblukcic.combinations;

import com.sblukcic.regions.RegionTag;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.biojava.nbio.core.sequence.DNASequence;

import java.util.Objects;

@AllArgsConstructor
@Getter
public class RegionCombination {

    RegionTag one;
    RegionTag two;
    DNASequence sequence;
    String id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegionCombination that = (RegionCombination) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
