package com.sblukcic.combinations;

import com.sblukcic.regions.RegionTag;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;

public class DefaultRegionCombinationFactory implements RegionCombinationFactory {
    @Override
    public RegionCombination create(RegionTag one, RegionTag two, String sequence, String id) throws CompoundNotFoundException {
        return new RegionCombination(one, two, new DNASequence(sequence), id);
    }
}
