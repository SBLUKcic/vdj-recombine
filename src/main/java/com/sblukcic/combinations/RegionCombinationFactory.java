package com.sblukcic.combinations;

import com.sblukcic.regions.RegionTag;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;

public interface RegionCombinationFactory {

    RegionCombination create(RegionTag one, RegionTag two, String sequence, String id) throws CompoundNotFoundException;
}
