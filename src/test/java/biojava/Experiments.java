package biojava;

import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.compound.AmbiguityDNACompoundSet;
import org.biojava.nbio.core.sequence.compound.AmbiguityRNACompoundSet;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.template.Sequence;
import org.biojava.nbio.core.sequence.template.SequenceView;
import org.biojava.nbio.core.sequence.transcription.Frame;
import org.biojava.nbio.core.sequence.transcription.TranscriptionEngine;
import org.biojava.nbio.genome.io.fastq.*;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class Experiments {

    @Test
    void creatingDNASequence() throws CompoundNotFoundException {

        String seq = "ATCG";

        DNASequence sequence = new DNASequence(seq);

        SequenceView reversed = sequence.getReverse();

        assertEquals(reversed.getViewedSequence().toString(), "ATCG");
        assertEquals(reversed.getSequenceAsString(), "GCTA");

        SequenceView rc = sequence.getReverseComplement();

        assertEquals(rc.getSequenceAsString(), "CGAT");
    }

    @Test
    void creatingAminoAcid() throws CompoundNotFoundException {

        DNASequence sequence = new DNASequence("ATCGTCTGCTGAT");

        AmbiguityDNACompoundSet dnaCompoundSet = AmbiguityDNACompoundSet.getDNACompoundSet();
        AmbiguityRNACompoundSet rnaCompoundSet = AmbiguityRNACompoundSet.getRNACompoundSet();
        TranscriptionEngine engine = new TranscriptionEngine
                .Builder()
                .trimStop(false) //needed to conserve * as the stop codon.
                .stopAtStopCodons(false)
                .dnaCompounds(dnaCompoundSet)
                .rnaCompounds(rnaCompoundSet)
                .build();

        Frame[] forwardFrames = Frame.getForwardFrames();

        Map<Frame, Sequence<AminoAcidCompound>> results = engine.multipleFrameTranslation(sequence, forwardFrames);

        assertEquals("IVC*", results.get(Frame.ONE).getSequenceAsString());

    }

    @Test
    void ReadingAFastQFile() throws IOException  {

        FastqReader reader = new SangerFastqReader(); //Sanger, Illumina and Solexa encoding available.

        InputStream inputStream = getClass().getResourceAsStream("XSBF487.35bp.vPE.hs_SBL_VDJ_region_mask_v1.PA.p.fr_del.fastq");

        Iterable<Fastq> iterable = reader.read(inputStream);

        int count = 0;

        for(Fastq f : iterable){
            assertNotNull(f);
            count++;
        }

        System.out.println(count);

    }

    @Test
    void streamingAFile() throws IOException {

        Readable readable = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("XSBF487.35bp.vPE.hs_SBL_VDJ_region_mask_v1.PA.p.fr_del.fastq")));

        ParseListener listener = new ParseListener() {

            List<Fastq> fastqs = new ArrayList<>();
            FastqBuilder builder = new FastqBuilder();

            @Override
            public void description(String description) throws IOException {
                builder.withDescription(description);
            }

            @Override
            public void sequence(String sequence) throws IOException {
                builder.withSequence(sequence);
            }

            @Override
            public void appendSequence(String sequence) throws IOException {
               builder.appendSequence(sequence);
            }

            @Override
            public void repeatDescription(String repeatDescription) throws IOException {

            }

            @Override
            public void quality(String quality) throws IOException {
                builder.withQuality(quality);
            }

            @Override
            public void appendQuality(String quality) throws IOException {
                builder.appendQuality(quality);
            }

            @Override
            public void complete() throws IOException {
                fastqs.add(builder.build());
                //System.out.println("fastq added, current size: " + fastqs.size());
            }
        };

        FastqReader reader = new SangerFastqReader();

        reader.parse(readable, listener);
    }
}
