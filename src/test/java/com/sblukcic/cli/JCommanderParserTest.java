package com.sblukcic.cli;

import com.beust.jcommander.ParameterException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class JCommanderParserTest {

    private Parser parser;

    @BeforeEach
    void setUp() {
        parser = new JCommanderParser();
    }

    @Test
    void givenValidFASTQFile_whenParsed_checkParsingCorrect(@TempDir Path path) throws IOException {

        Path fastq = path.resolve("example.fastq");

        List<String> lines = Arrays.asList(
                "@MYID1",
                "ATCGTGTGTGCTGC",
                "+",
                "@@@@@@@@@@@@@@"
        );

        Files.write(fastq, lines);

        String[] args = new String[]{"-m", "coding",
                "-if", fastq.toString(),
                "-umi",
                "--umi-mismatch=10",
                "-d", "hello" };

        ProgramArgs pArgs = parser.parse(args);

        assertEquals(Mode.CODING, pArgs.getMode());
        assertEquals(fastq, pArgs.getInputFilePath());
        assertTrue(pArgs.isCheckUMIs());
        assertEquals(10, pArgs.getAllowedUMIMismatches());
        assertFalse(pArgs.isHelp());
        assertEquals(Paths.get(System.getProperty("user.dir") + File.separator + "hello").toAbsolutePath().toString(),
                pArgs.getDirectory().toFile().getAbsolutePath());

    }

    @Test
    void givenHelpCalled_whenParsed_checkExceptionThrown() {

        ParameterException exc = assertThrows(ParameterException.class,
                () -> parser.parse(new String[]{ "-h" }));

        assertEquals("Help Called", exc.getMessage());
    }
}