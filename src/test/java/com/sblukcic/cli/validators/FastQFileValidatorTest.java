package com.sblukcic.cli.validators;

import com.beust.jcommander.ParameterException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FastQFileValidatorTest {

    private FastQFileValidator validator;

    @BeforeEach
    void setUp() {
        validator = new FastQFileValidator();
    }

    @Test
    void givenValidFASTQFile_whenValidated_checkNoExceptionThrown(@TempDir Path path) throws IOException {

        Path fastq = path.resolve("example.fastq");

        List<String> lines = Arrays.asList(
                "@MYID1",
                "ATCGTGTGTGCTGC",
                "+",
                "@@@@@@@@@@@@@@"
        );

        Files.write(fastq, lines);

        validator.validate("command", fastq.toString());

    }

    @Test
    void givenEmptyFASTQFile_whenValidated_checkNoExceptionThrown(@TempDir Path path) throws IOException {

        Path fastq = path.resolve("example.fastq");

        List<String> lines = Collections.singletonList("");

        Files.write(fastq, lines);

        ParameterException exception = assertThrows(ParameterException.class,
                () -> validator.validate("command", fastq.toString()));

        assertEquals("Error reading requested input file: description must begin with a '@' character", exception.getMessage());

    }

    @Test
    void givenInvalidFASTQFile_whenValidated_checkNoExceptionThrown(@TempDir Path path) throws IOException {

        Path fastq = path.resolve("example.fastq");

        List<String> lines = Arrays.asList(
                "@MYID1",
                "ATCGTGTGTGCTGC",
                "+"
        );

        Files.write(fastq, lines);

        ParameterException exception = assertThrows(ParameterException.class,
            () -> validator.validate("command", fastq.toString()));

        assertEquals("Error reading requested input file: truncated sequence", exception.getMessage());

    }

    @Test
    void givenInvalidFASTQFilePath_whenValidated_checkNoExceptionThrown() throws IOException {

        ParameterException exception = assertThrows(ParameterException.class,
                () -> validator.validate("command", "@yo"));

        assertEquals("Path @yo doesn't exist", exception.getMessage());

    }
}