package com.sblukcic.cli;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class ProgramArgsTest {

    private ProgramArgs args;

    @BeforeEach
    void setUp() {
        args = new ProgramArgs();
    }

    @Test
    void givenDefaultProgramArgs_checkDefaultsCorrect() {

        assertNull(args.getInputFilePath());
        assertEquals(Paths.get(System.getProperty("user.dir")), args.getDirectory().getParent());
        assertEquals(Mode.SIGNAL, args.getMode());
        assertEquals(0, args.getAllowedUMIMismatches());
        assertFalse(args.isHelp());
        assertFalse(args.isCheckUMIs());
    }
}