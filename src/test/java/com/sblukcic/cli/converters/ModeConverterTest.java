package com.sblukcic.cli.converters;

import com.sblukcic.cli.Mode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ModeConverterTest {

    private ModeConverter converter;

    @BeforeEach
    void setUp() {
        converter = new ModeConverter();
    }

    @Test
    void givenSignalChosenLowerCase_whenConverted_checkCorrect() {

        Mode m = converter.convert("signal");

        assertEquals(Mode.SIGNAL, m);
    }

    @Test
    void givenSignalChosenMixedCase_whenConverted_checkCorrect() {

        Mode m = converter.convert("SiGnAL");

        assertEquals(Mode.SIGNAL, m);
    }

    @Test
    void givenCodingChosenLowerCase_whenConverted_checkCorrect() {

        Mode m = converter.convert("coding");

        assertEquals(Mode.CODING, m);
    }

    @Test
    void givenCodingChosenMixedCase_whenConverted_checkCorrect() {

        Mode m = converter.convert("CoDiNg");

        assertEquals(Mode.CODING, m);
    }

    @Test
    void givenInvalidString_whenConverted_checkSignalChosenByDefault() {

        Mode m = converter.convert("HELLO!");

        assertEquals(Mode.SIGNAL, m);
    }
}