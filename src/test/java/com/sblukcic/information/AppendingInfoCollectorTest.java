package com.sblukcic.information;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class AppendingInfoCollectorTest {

    private InfoCollector infoCollector;

    @BeforeEach
    void setUp() {
        infoCollector = new AppendingInfoCollector();
    }

    @Test
    void givenValidSectionAndLines_whenWritten_checkCorrect() throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        infoCollector.addInfo("Section One",
                "This is some info",
                "About stuff that has happened");

        infoCollector.write(baos);

        String output = baos.toString();

        System.out.println(output);

        assertTrue(output.contains("Section One"));
        assertTrue(output.contains("This is some info"));
        assertTrue(output.contains("About stuff that has happened"));
    }
}