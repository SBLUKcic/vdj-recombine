package com.sblukcic.combinations;

import com.sblukcic.combinations.threads.CombinationFinderThreadFactory;
import com.sblukcic.information.InfoCollector;
import com.sblukcic.regions.RegionTag;
import org.biojava.nbio.core.sequence.DNASequence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ConcurrentCombinationFinderTest {

    @Mock
    private CombinationFinderThreadFactory threadFactoryMock;

    @Mock
    private InfoCollector collector;

    @Mock
    private Callable<List<RegionCombination>> callMock;

    private CombinationFinder finder;

    @Mock
    private RegionTag one;

    @Mock
    private RegionTag two;

    private int cores  = Runtime.getRuntime().availableProcessors();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void givenValidFastqFile_whenCombinationsSearched_checkDistributedToThreads(@TempDir Path path) throws Exception {
        finder = new ConcurrentCombinationFinder(threadFactoryMock, collector, 500, cores);
        Path fastq = path.resolve("temp.fastq");

        List<String> lines = Arrays.asList(
                "@ID1",
                "ATCGTCTGTGTGCTGTCTGTG",
                "+",
                "@@@@@@@@@@@@@@@@@@@@@",
                "@ID2",
                "ATCGTCTGTGTGCTGTCTGTG",
                "+",
                "@@@@@@@@@@@@@@@@@@@@@",
                "@ID3",
                "ATCGTCTGTGTGCTGTCTGTG",
                "+",
                "@@@@@@@@@@@@@@@@@@@@@",
                "@ID4",
                "ATCGTCTGTGTGCTGTCTGTG",
                "+",
                "@@@@@@@@@@@@@@@@@@@@@"
        );

        Files.write(fastq, lines);

        when(threadFactoryMock.create(any())).thenReturn(callMock);

        List<RegionCombination> combos = finder.find(fastq, new ArrayList<>());

        verify(callMock, times(1)).call();

    }

    @Test
    void givenValidFastqFileWithExclusions_whenCombinationsSearched_checkDistributedToThreads(@TempDir Path path) throws Exception {
        finder = new ConcurrentCombinationFinder(threadFactoryMock, collector, 1, cores);
        Path fastq = path.resolve("temp.fastq");

        List<String> lines = Arrays.asList(
                "@ID1",
                "ATCGTCTGTGTGCTGTCTGTG",
                "+",
                "@@@@@@@@@@@@@@@@@@@@@",
                "@ID2",
                "ATCGTCTGTGTGCTGTCTGTG",
                "+",
                "@@@@@@@@@@@@@@@@@@@@@",
                "@ID3",
                "ATCGTCTGTGTGCTGTCTGTG",
                "+",
                "@@@@@@@@@@@@@@@@@@@@@",
                "@ID4",
                "ATCGTCTGTGTGCTGTCTGTG",
                "+",
                "@@@@@@@@@@@@@@@@@@@@@"
        );

        Files.write(fastq, lines);

        when(threadFactoryMock.create(any())).thenReturn(callMock);

        List<RegionCombination> combos = finder.find(fastq, Arrays.asList("@ID1", "@ID2"));

        verify(callMock, times(2)).call();

    }

    @Test
    void givenValidFastqFile_whenCombinationsSearched_checkReturnedFromThreads(@TempDir Path path) throws Exception {
        finder = new ConcurrentCombinationFinder(threadFactoryMock, collector,500, cores);
        Path fastq = path.resolve("temp.fastq");

        List<String> lines = Arrays.asList(
                "@ID1",
                "ATCGTCTGTGTGCTGTCTGTG",
                "+",
                "@@@@@@@@@@@@@@@@@@@@@",
                "@ID2",
                "ATCGTCTGTGTGCTGTCTGTG",
                "+",
                "@@@@@@@@@@@@@@@@@@@@@",
                "@ID3",
                "ATCGTCTGTGTGCTGTCTGTG",
                "+",
                "@@@@@@@@@@@@@@@@@@@@@",
                "@ID4",
                "ATCGTCTGTGTGCTGTCTGTG",
                "+",
                "@@@@@@@@@@@@@@@@@@@@@"
        );

        Files.write(fastq, lines);

        when(threadFactoryMock.create(any())).thenReturn(callMock);

        RegionCombination combo = new RegionCombination(one, two, new DNASequence("ATCG"), "@ID2");

        when(callMock.call()).thenReturn(Arrays.asList(
                combo
        ));
        List<RegionCombination> combos = finder.find(fastq, new ArrayList<>());

        verify(callMock, times(1)).call();
        assertEquals(1, combos.size());
        assertEquals(combo, combos.get(0));

    }

    @Test
    void givenValidFastqFile_whenCombinationsSearchedWithOneFastqPerThread_checkReturnedFromThreads(@TempDir Path path) throws Exception {
        finder = new ConcurrentCombinationFinder(threadFactoryMock, collector,1, cores);
        Path fastq = path.resolve("temp.fastq");

        List<String> lines = Arrays.asList(
                "@ID1",
                "ATCGTCTGTGTGCTGTCTGTG",
                "+",
                "@@@@@@@@@@@@@@@@@@@@@",
                "@ID2",
                "ATCGTCTGTGTGCTGTCTGTG",
                "+",
                "@@@@@@@@@@@@@@@@@@@@@",
                "@ID3",
                "ATCGTCTGTGTGCTGTCTGTG",
                "+",
                "@@@@@@@@@@@@@@@@@@@@@",
                "@ID4",
                "ATCGTCTGTGTGCTGTCTGTG",
                "+",
                "@@@@@@@@@@@@@@@@@@@@@"
        );

        Files.write(fastq, lines);

        when(threadFactoryMock.create(any())).thenReturn(callMock);

        RegionCombination combo = new RegionCombination(one, two, new DNASequence("ATCG"), "@ID2");

        when(callMock.call()).thenReturn(Arrays.asList(
                combo
        ));
        List<RegionCombination> combos = finder.find(fastq, new ArrayList<>());

        verify(callMock, times(4)).call();
        assertEquals(4, combos.size());
        assertEquals(combo, combos.get(3));

    }
}