package com.sblukcic.combinations;

import com.sblukcic.regions.RegionTag;
import com.sblukcic.regions.RegionTagFactory;
import com.sblukcic.regions.TRRegionTagFactory;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RegionCombinationTest {

    private RegionTagFactory tagFactory;

    @BeforeEach
    void setUp() {
        tagFactory = new TRRegionTagFactory();
    }

    @Test
    void givenValidTags_whenBuilt_checkCorrect() throws CompoundNotFoundException {

        RegionTag one = tagFactory.createRegionTag("TRAJ2", "ATCGTG", "TGTGTG");
        RegionTag two = tagFactory.createRegionTag("TRAV2", "ATCGTG", "TGTGTG");

        RegionCombination combo = new RegionCombination(one, two, new DNASequence("ATCG"), "@id1");

        assertEquals("CGAT", combo.getSequence().getReverseComplement().getSequenceAsString());
    }

    @Test
    void givenValidTagsWithDifferentIDs_whenBuild_checkEquality() throws CompoundNotFoundException {

        RegionTag one = tagFactory.createRegionTag("TRAJ2", "ATCGTG", "TGTGTG");
        RegionTag two = tagFactory.createRegionTag("TRAV2", "ATCGTG", "TGTGTG");

        RegionCombination combo = new RegionCombination(one, two, new DNASequence("ATCG"), "@id1");

        RegionCombination comboTwo = new RegionCombination(one, two, new DNASequence("ATCG"), "@id2");

        assertNotEquals(combo, comboTwo);
    }

    @Test
    void givenValidTagsWithSameID_whenBuild_checkEquality() throws CompoundNotFoundException {

        RegionTag one = tagFactory.createRegionTag("TRAJ2", "ATCGTG", "TGTGTG");
        RegionTag two = tagFactory.createRegionTag("TRAV2", "ATCGTG", "TGTGTG");

        RegionCombination combo = new RegionCombination(one, two, new DNASequence("ATCG"), "@id1");
        RegionCombination comboTwo = new RegionCombination(one, two, new DNASequence("ATCTGTGTGG"), "@id1");

        assertEquals(combo, comboTwo);
        assertEquals(one, combo.getOne());
        assertEquals(two, combo.getTwo());
        assertEquals("@id1", combo.getId());
    }

    @Test
    void hashCodeTest() throws CompoundNotFoundException {
        RegionTag one = tagFactory.createRegionTag("TRAJ2", "ATCGTG", "TGTGTG");
        RegionTag two = tagFactory.createRegionTag("TRAV2", "ATCGTG", "TGTGTG");

        RegionCombination combo = new RegionCombination(one, two, new DNASequence("ATCG"), "@id1");

        assertEquals(2010709, combo.hashCode());
    }
}