package com.sblukcic.combinations;

import com.sblukcic.regions.Locus;
import com.sblukcic.regions.RegionTag;
import com.sblukcic.regions.RegionTagFactory;
import org.biojava.nbio.core.sequence.DNASequence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DefaultRegionCombinationFactoryTest {

    private RegionCombinationFactory factory;

    //test factories
    private RegionTagFactory rtFac = (name, sequence, rssBases) -> new RegionTag(
            name, new DNASequence(sequence), Locus.ALPHA, new DNASequence(rssBases),false
    );

    @BeforeEach
    void setUp() {
        factory = new DefaultRegionCombinationFactory();
    }

    @Test
    void givenCorrectTags_whenRegionCombinationCreated_checkCorrect() throws Exception {

        RegionTag one = rtFac.createRegionTag("ONE", "ATCG", "TTGC");
        RegionTag two = rtFac.createRegionTag("TWO", "TTGC", "ATCG");

        RegionCombination combo = factory.create(one, two, "ATCG", "@Id1");

        assertEquals(one, combo.getOne());
        assertEquals(two, combo.getTwo());
        assertEquals("ATCG", combo.getSequence().getSequenceAsString());
        assertEquals("@Id1", combo.getId());


    }
}