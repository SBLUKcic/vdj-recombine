package com.sblukcic.combinations.threads;

import com.sblukcic.algorithms.SearchAlgorithmFactory;
import com.sblukcic.combinations.RegionCombination;
import com.sblukcic.combinations.RegionCombinationFactory;
import com.sblukcic.regions.CachingFastaRegionProvider;
import com.sblukcic.regions.REGION_TAG_TYPE;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class DefaultCombinationFinderThreadFactoryTest {

    @Mock
    private CachingFastaRegionProvider provider;

    @Mock
    private SearchAlgorithmFactory algorithmFactory;

    @Mock
    private RegionCombinationFactory regionCombinationFactory;

    private RegionTagTypeAwareComboFinderThreadFactory factory;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        factory = new DefaultCombinationFinderThreadFactory(provider,
                algorithmFactory,
                regionCombinationFactory);
    }

    @Test
    void givenDefaultSetup_checkCallableCreatedCorrectly() throws Exception {

        when(provider.getRegionsForTagType(REGION_TAG_TYPE.J_SIGNAL)).thenReturn(new ArrayList<>());
        when(provider.getRegionsForTagType(REGION_TAG_TYPE.V_SIGNAL)).thenReturn(new ArrayList<>());

        Callable<List<RegionCombination>> callable = factory.create(new ArrayList<>());

        assertNotNull(callable);

    }

    @Test
    void givenDefaultSetup_whenRegionTypesChanged_checkCallableCreatedCorrectly() throws Exception {

        factory.setCurrentType(REGION_TAG_TYPE.J_CODING, REGION_TAG_TYPE.V_CODING);

        when(provider.getRegionsForTagType(REGION_TAG_TYPE.J_CODING)).thenReturn(new ArrayList<>());
        when(provider.getRegionsForTagType(REGION_TAG_TYPE.V_CODING)).thenReturn(new ArrayList<>());

        Callable<List<RegionCombination>> callable = factory.create(new ArrayList<>());

        assertNotNull(callable);

    }
}