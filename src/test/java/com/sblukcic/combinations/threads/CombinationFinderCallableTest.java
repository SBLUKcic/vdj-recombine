package com.sblukcic.combinations.threads;

import com.sblukcic.algorithms.SearchAlgorithm;
import com.sblukcic.algorithms.SearchAlgorithmFactory;
import com.sblukcic.combinations.RegionCombination;
import com.sblukcic.combinations.RegionCombinationFactory;
import com.sblukcic.regions.Locus;
import com.sblukcic.regions.RegionTag;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.genome.io.fastq.Fastq;
import org.biojava.nbio.genome.io.fastq.FastqBuilder;
import org.biojava.nbio.genome.io.fastq.FastqVariant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class CombinationFinderCallableTest {

    private List<Fastq> fastqs;
    private FastqBuilder builder;
    @Mock
    private SearchAlgorithmFactory searchAlgorithmFactory;

    @Mock
    private RegionCombinationFactory regionCombinationFactory;

    @Mock
    private SearchAlgorithm algorithm;

    @Mock
    private SearchAlgorithm anotherAlgorithm;

    @Mock
    private SearchAlgorithm repAlg;

    @Mock
    private RegionCombination combination;

    private String sequence = "AAAATTTTCCCCGGGG";

    @BeforeEach
    void setUp() {
        builder = new FastqBuilder();
        fastqs = new ArrayList<>();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void givenOneFastqRecordandValidNonRepTags_whenCombosSearched_checkComboCaptured() throws Exception {
        fastqs.add(builder
                .withDescription("@ID1")
                .withSequence(sequence)
                .withVariant(FastqVariant.FASTQ_ILLUMINA)
                .withQuality("@@@@@@@@@@@@@@@@")
                .build()
        );
        List<RegionTag> one = Collections.singletonList(new RegionTag("REGION_1",
                new DNASequence("AATT"),
                Locus.ALPHA,
                new DNASequence("ATGC"),
                false));

        List<RegionTag> two = Collections.singletonList(new RegionTag("REGION_2",
                new DNASequence("GGGG"),
                Locus.ALPHA,
                new DNASequence("ATGC"),
                false));

        Callable<List<RegionCombination>> callable = new CombinationFinderCallable(fastqs,
                one,
                two,
                searchAlgorithmFactory,
                regionCombinationFactory);



        when(searchAlgorithmFactory.createSearchAlgorithm("AATT")).thenReturn(algorithm);
        when(algorithm.search(sequence)).thenReturn(2);

        when(searchAlgorithmFactory.createSearchAlgorithm("GGGG")).thenReturn(anotherAlgorithm);
        when(anotherAlgorithm.search(sequence)).thenReturn(12);

        when(regionCombinationFactory.create(one.get(0), two.get(0), "TTCCCC", "@ID1")).thenReturn(combination);

        List<RegionCombination> combos = callable.call();

        assertEquals(1, combos.size());

    }

    @Test
    void givenOneFastqRecordandValidNonRepTagsButDifferentLoci_whenCombosSearched_checkComboNotCaptured() throws Exception {
        fastqs.add(builder
                .withDescription("@ID1")
                .withSequence(sequence)
                .withVariant(FastqVariant.FASTQ_ILLUMINA)
                .withQuality("@@@@@@@@@@@@@@@@")
                .build()
        );
        List<RegionTag> one = Collections.singletonList(new RegionTag("REGION_1",
                new DNASequence("AATT"),
                Locus.BETA,
                new DNASequence("ATGC"),
                false));

        List<RegionTag> two = Collections.singletonList(new RegionTag("REGION_2",
                new DNASequence("GGGG"),
                Locus.ALPHA,
                new DNASequence("ATGC"),
                false));

        Callable<List<RegionCombination>> callable = new CombinationFinderCallable(fastqs,
                one,
                two,
                searchAlgorithmFactory,
                regionCombinationFactory);



        when(searchAlgorithmFactory.createSearchAlgorithm("AATT")).thenReturn(algorithm);
        when(algorithm.search(sequence)).thenReturn(2);

        when(searchAlgorithmFactory.createSearchAlgorithm("GGGG")).thenReturn(anotherAlgorithm);
        when(anotherAlgorithm.search(sequence)).thenReturn(12);

        when(regionCombinationFactory.create(one.get(0), two.get(0), "TTCCCC", "@ID1")).thenReturn(combination);

        List<RegionCombination> combos = callable.call();

        assertEquals(0, combos.size());

    }

    @Test
    void givenOneFastqRecordandValidNonRepTags_whenCombosSearchedButNoneMatch_checkNoComboCaptured() throws Exception {
        fastqs.add(builder
                .withDescription("@ID1")
                .withSequence(sequence)
                .withVariant(FastqVariant.FASTQ_ILLUMINA)
                .withQuality("@@@@@@@@@@@@@@@@")
                .build()
        );
        List<RegionTag> one = Collections.singletonList(new RegionTag("REGION_1",
                new DNASequence("AGTGTGTGTG"),
                Locus.ALPHA,
                new DNASequence("ATGC"),
                false));

        List<RegionTag> two = Collections.singletonList(new RegionTag("REGION_2",
                new DNASequence("GGCTCTCTCG"),
                Locus.ALPHA,
                new DNASequence("ATGC"),
                false));

        Callable<List<RegionCombination>> callable = new CombinationFinderCallable(fastqs,
                one,
                two,
                searchAlgorithmFactory,
                regionCombinationFactory);

        String reverse = new DNASequence(sequence).getReverseComplement().getSequenceAsString();

        when(searchAlgorithmFactory.createSearchAlgorithm("AGTGTGTGTG")).thenReturn(algorithm);
        when(searchAlgorithmFactory.createSearchAlgorithm(new DNASequence("AGTGTGTGTG").getReverseComplement().getSequenceAsString())).thenReturn(algorithm);
        when(algorithm.search(sequence)).thenReturn(-1);
        when(algorithm.search(reverse)).thenReturn(-1);

        when(searchAlgorithmFactory.createSearchAlgorithm("GGCTCTCTCG")).thenReturn(anotherAlgorithm);
        when(searchAlgorithmFactory.createSearchAlgorithm(new DNASequence("GGCTCTCTCG").getReverseComplement().getSequenceAsString())).thenReturn(anotherAlgorithm);
        when(anotherAlgorithm.search(sequence)).thenReturn(-1);
        when(anotherAlgorithm.search(reverse)).thenReturn(-1);

        List<RegionCombination> combos = callable.call();

        assertEquals(0, combos.size());

    }

    @Test
    void givenOneFastqRecordandValidNonRepTagsReverseOrderList_whenCombosSearched_checkComboCaptured() throws Exception {
        fastqs.add(builder
                .withDescription("@ID1")
                .withSequence(sequence)
                .withVariant(FastqVariant.FASTQ_ILLUMINA)
                .withQuality("@@@@@@@@@@@@@@@@")
                .build()
        );
        List<RegionTag> one = Collections.singletonList(new RegionTag("REGION_1",
                new DNASequence("AATT"),
                Locus.ALPHA,
                new DNASequence("ATGC"),
                false));

        List<RegionTag> two = Collections.singletonList(new RegionTag("REGION_2",
                new DNASequence("GGGG"),
                Locus.ALPHA,
                new DNASequence("ATGC"),
                false));

        Callable<List<RegionCombination>> callable = new CombinationFinderCallable(fastqs,
                two,
                one,
                searchAlgorithmFactory,
                regionCombinationFactory);

        when(searchAlgorithmFactory.createSearchAlgorithm("AATT")).thenReturn(algorithm);
        when(algorithm.search(sequence)).thenReturn(2);

        when(searchAlgorithmFactory.createSearchAlgorithm("GGGG")).thenReturn(anotherAlgorithm);
        when(anotherAlgorithm.search(sequence)).thenReturn(12);

        when(regionCombinationFactory.create(one.get(0), two.get(0), "TTCCCC", "@ID1")).thenReturn(combination);

        List<RegionCombination> combos = callable.call();

        assertEquals(1, combos.size());

    }

    @Test
    void givenOneFastqRecordandValidRepTags_whenCombosSearched_checkNonAltComboCaptured() throws Exception {
        fastqs.add(builder
                .withDescription("@ID1")
                .withSequence(sequence)
                .withVariant(FastqVariant.FASTQ_ILLUMINA)
                .withQuality("@@@@@@@@@@@@@@@@")
                .build()
        );

        RegionTag repOne = new RegionTag("rTAG(REGION_1)",
                new DNASequence("AATT"),
                Locus.ALPHA,
                new DNASequence("ATGC"),
                true);

        RegionTag repetitiveTag = new RegionTag("REGION_1",
                new DNASequence("AAAA"),
                Locus.ALPHA,
                new DNASequence("ATGC"),
                false);
        repetitiveTag.addAlternativeTag(repOne);

        List<RegionTag> one = Collections.singletonList(repetitiveTag);

        List<RegionTag> two = Collections.singletonList(new RegionTag("REGION_2",
                new DNASequence("GGGG"),
                Locus.ALPHA,
                new DNASequence("ATGC"),
                false));

        Callable<List<RegionCombination>> callable = new CombinationFinderCallable(fastqs,
                two,
                one,
                searchAlgorithmFactory,
                regionCombinationFactory);

        when(searchAlgorithmFactory.createSearchAlgorithm("AAAA")).thenReturn(repAlg);
        when(repAlg.search(sequence)).thenReturn(0);

        when(searchAlgorithmFactory.createSearchAlgorithm("AATT")).thenReturn(algorithm);
        when(algorithm.search(sequence)).thenReturn(2);

        when(searchAlgorithmFactory.createSearchAlgorithm("GGGG")).thenReturn(anotherAlgorithm);
        when(anotherAlgorithm.search(sequence)).thenReturn(12);

        when(regionCombinationFactory.create(one.get(0), two.get(0), "TTTTCCCC", "@ID1")).thenReturn(combination);

        List<RegionCombination> combos = callable.call();

        assertEquals(1, combos.size());
        assertEquals(combination, combos.get(0));

    }

    @Test
    void givenOneFastqRecordInReverseComplimentandValidNonRepTags_whenCombosSearched_checkComboCaptured() throws Exception {

        DNASequence forward = new DNASequence("AATTCCGGGGCCTTAA");
        String reverse = forward.getReverseComplement().getSequenceAsString().toString();

        fastqs.add(builder
                .withDescription("@ID1")
                .withSequence(forward.getSequenceAsString()) //rv = TTAAGGCCCCGGAATT
                .withVariant(FastqVariant.FASTQ_ILLUMINA)
                .withQuality("@@@@@@@@@@@@@@@@")
                .build()
        );
        List<RegionTag> one = Collections.singletonList(new RegionTag("REGION_1",
                new DNASequence("AAGG"),
                Locus.ALPHA,
                new DNASequence("ATGC"),
                false));

        List<RegionTag> two = Collections.singletonList(new RegionTag("REGION_2",
                new DNASequence("GGAA"),
                Locus.ALPHA,
                new DNASequence("ATGC"),
                false));

        Callable<List<RegionCombination>> callable = new CombinationFinderCallable(fastqs,
                one,
                two,
                searchAlgorithmFactory,
                regionCombinationFactory);

        when(searchAlgorithmFactory.createSearchAlgorithm("AAGG")).thenReturn(algorithm);
        when(algorithm.search(forward.getSequenceAsString())).thenReturn(-1);
        when(algorithm.search(reverse)).thenReturn(2);

        when(searchAlgorithmFactory.createSearchAlgorithm("GGAA")).thenReturn(anotherAlgorithm);
        when(anotherAlgorithm.search(forward.getSequenceAsString())).thenReturn(-1);
        when(anotherAlgorithm.search(reverse)).thenReturn(10);
        when(regionCombinationFactory.create(one.get(0), two.get(0), "CCCC", "@ID1")).thenReturn(combination);

        List<RegionCombination> combos = callable.call();

        assertEquals(1, combos.size());

    }
}