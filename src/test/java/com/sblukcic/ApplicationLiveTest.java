package com.sblukcic;

import org.junit.jupiter.api.Test;

class ApplicationLiveTest {

    @Test
    void givenNoInputArgument_whenMainRun_expectErrorPrinted() {
        Application.main(new String[]{});
    }

    @Test
    void givenHelpArgument_whenMainRun_expectHelpPrinted(){
        Application.main(new String[]{"-h"});
    }
}