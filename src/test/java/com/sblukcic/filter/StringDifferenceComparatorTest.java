package com.sblukcic.filter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StringDifferenceComparatorTest {

    private Comparator<String> comparator;

    @BeforeEach
    void setUp() {
        comparator = new StringDifferenceComparator();
    }

    @Test
    void givenSameString_whenCompared_check0Returned() {

        assertEquals(0, comparator.compare("ACGTGCTG", "ACGTGCTG"));
    }

    @Test
    void givenStringwith1MM_whenCompared_check1Returned() {
        assertEquals(1, comparator.compare("ATCG", "ATCT"));
    }

    @Test
    void givenStringwith2MM_whenCompared_check1Returned() {
        assertEquals(2, comparator.compare("ATCG", "ATTT"));
    }

    @Test
    void givenStringwithAllMM_whenCompared_check1Returned() {
        assertEquals(4, comparator.compare("TATA", "ATCT"));
    }

    @Test
    void givenStringsDiffLength_whenCompared_checkMinus1Returned() {
        assertEquals(-1, comparator.compare("A", "AA"));
    }
}