package com.sblukcic.filter;

import com.sblukcic.aggregation.ComboAggregation;
import com.sblukcic.aggregation.VariableSequence;
import com.sblukcic.regions.RegionTag;
import org.biojava.nbio.core.sequence.DNASequence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UMIFilterWithMismatchTest {

    private GenericFilter<List<ComboAggregation>> filter;

    @Mock
    private RegionTag tag;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        filter = new UMIFilterWithMismatch(new StringDifferenceComparator());
    }

    @Test
    void givenTwoReadsInSameComboWithSameBCRs_whenFiltered_checkOneRemoved() throws Exception{

        ComboAggregation agg = new ComboAggregation(tag, tag);
        VariableSequence seq = new VariableSequence(new DNASequence("ATCG"), new ArrayList<>());
        seq.addId("@SBL_ID:227:BCR1:CGCAGA:BCR2:ATCTAA/1");
        seq.addId("@SBL_ID:228:BCR1:CGCAGA:BCR2:ATCTAA/1");

        agg.addVariableSequence(seq);

        List<ComboAggregation> filtered = filter.filter(Collections.singletonList(agg));

        assertEquals(1, filtered.get(0).getVariableSequences().get(0).getIds().size());

    }

    @Test
    void givenTwoReadsInSameComboWithDifferentBCRs_whenFiltered_checkBothKept() throws Exception{

        ComboAggregation agg = new ComboAggregation(tag, tag);
        VariableSequence seq = new VariableSequence(new DNASequence("ATCG"), new ArrayList<>());
        seq.addId("@SBL_ID:227:BCR1:CGGTTT:BCR2:ATTTAA/1");
        seq.addId("@SBL_ID:228:BCR1:CGCAGA:BCR2:ATCTAA/1");

        agg.addVariableSequence(seq);

        List<ComboAggregation> filtered = filter.filter(Collections.singletonList(agg));

        assertEquals(2, filtered.get(0).getVariableSequences().get(0).getIds().size());

    }

    @Test
    void givenTwoReadsInSameComboWithDifferentBCRsWithNoFastQEnding_whenFiltered_checkBothKept() throws Exception{

        ComboAggregation agg = new ComboAggregation(tag, tag);
        VariableSequence seq = new VariableSequence(new DNASequence("ATCG"), new ArrayList<>());
        seq.addId("@SBL_ID:227:BCR1:CGGTTT:BCR2:ATTTAA");
        seq.addId("@SBL_ID:228:BCR1:CGCAGA:BCR2:ATCTAA");

        agg.addVariableSequence(seq);

        List<ComboAggregation> filtered = filter.filter(Collections.singletonList(agg));

        assertEquals(2, filtered.get(0).getVariableSequences().get(0).getIds().size());

    }

    @Test
    void givenTwoReadsInSameComboWithSameBCRsWithNoFastqEnding_whenFiltered_checkOneRemoved() throws Exception{

        ComboAggregation agg = new ComboAggregation(tag, tag);
        VariableSequence seq = new VariableSequence(new DNASequence("ATCG"), new ArrayList<>());
        seq.addId("@SBL_ID:227:BCR1:CGCAGA:BCR2:ATCTAA");
        seq.addId("@SBL_ID:228:BCR1:CGCAGA:BCR2:ATCTAA");

        agg.addVariableSequence(seq);

        List<ComboAggregation> filtered = filter.filter(Collections.singletonList(agg));

        assertEquals(1, filtered.get(0).getVariableSequences().get(0).getIds().size());

    }

    //not sure if this combination is a real scenario - but let's accoutn for it anyway.
    @Test
    void givenTwoReadsInSameComboWithSameBCRsDifferentOrder_whenFiltered_checkOneRemoved() throws Exception{

        ComboAggregation agg = new ComboAggregation(tag, tag);
        VariableSequence seq = new VariableSequence(new DNASequence("ATCG"), new ArrayList<>());
        seq.addId("@SBL_ID:227:BCR1:CGGTTT:BCR2:ATTTAA/1");
        seq.addId("@SBL_ID:228:BCR1:ATTTAA:BCR2:CGGTTT/1");

        agg.addVariableSequence(seq);

        List<ComboAggregation> filtered = filter.filter(Collections.singletonList(agg));

        assertEquals(1, filtered.get(0).getVariableSequences().get(0).getIds().size());

    }

    @Test
    void givenTwoReadsInSameComboWithSameBCRsReverseCompliment_whenFiltered_checkOneRemoved() throws Exception{

        DNASequence bcr1 = new DNASequence("CGGTTT");
        DNASequence bcr2 = new DNASequence("ATTTAA");
        ComboAggregation agg = new ComboAggregation(tag, tag);
        VariableSequence seq = new VariableSequence(new DNASequence("ATCG"), new ArrayList<>());
        seq.addId(String.format("@SBL_ID:227:BCR1:%s:BCR2:%s/1",
                bcr1.getSequenceAsString(),
                bcr2.getSequenceAsString()));

        seq.addId(String.format("@SBL_ID:227:BCR1:%s:BCR2:%s/1",
                bcr2.getReverseComplement().getSequenceAsString(),
                bcr1.getReverseComplement().getSequenceAsString()));

        agg.addVariableSequence(seq);

        List<ComboAggregation> filtered = filter.filter(Collections.singletonList(agg));

        assertEquals(1, filtered.get(0).getVariableSequences().get(0).getIds().size());

    }

    @Test
    void givenTwoReadsInSameComboWithOneBPDifference_whenFilteredWith0BPMismatch_checkNoneRemoved() throws Exception{

        ComboAggregation agg = new ComboAggregation(tag, tag);
        VariableSequence seq = new VariableSequence(new DNASequence("ATCG"), new ArrayList<>());
        seq.addId("@SBL_ID:227:BCR1:CGCAGA:BCR2:ATCTAA/1");
        seq.addId("@SBL_ID:228:BCR1:CGCGGA:BCR2:ATCTAA/1");

        agg.addVariableSequence(seq);

        List<ComboAggregation> filtered = filter.filter(Collections.singletonList(agg));

        assertEquals(2, filtered.get(0).getVariableSequences().get(0).getIds().size());

    }

    @Test
    void givenTwoReadsInSameComboWithOneBPDifference_whenFilteredWith1BPMismatch_checkOneRemoved() throws Exception{

        ComboAggregation agg = new ComboAggregation(tag, tag);
        VariableSequence seq = new VariableSequence(new DNASequence("ATCG"), new ArrayList<>());
        seq.addId("@SBL_ID:227:BCR1:CGCAGA:BCR2:ATCTAA/1");
        seq.addId("@SBL_ID:228:BCR1:CGCGGA:BCR2:ATCTAA/1");

        agg.addVariableSequence(seq);

        List<ComboAggregation> filtered = filter.filter(Collections.singletonList(agg), 1);

        assertEquals(1, filtered.get(0).getVariableSequences().get(0).getIds().size());

    }

    @Test
    void givenTwoReadsInSameComboWithOneBPDifference_whenFilteredWith2BPMismatch_checkOneRemoved() throws Exception{

        ComboAggregation agg = new ComboAggregation(tag, tag);
        VariableSequence seq = new VariableSequence(new DNASequence("ATCG"), new ArrayList<>());
        seq.addId("@SBL_ID:227:BCR1:CGCAGA:BCR2:ATCTAA/1");
        seq.addId("@SBL_ID:228:BCR1:CGCGGA:BCR2:ATCTAA/1");

        agg.addVariableSequence(seq);

        List<ComboAggregation> filtered = filter.filter(Collections.singletonList(agg), 2);

        assertEquals(1, filtered.get(0).getVariableSequences().get(0).getIds().size());

    }

    @Test
    void givenTwoReadsInSameComboWithTwoBPDifference_whenFilteredWith2BPMismatch_checkOneRemoved() throws Exception{

        ComboAggregation agg = new ComboAggregation(tag, tag);
        VariableSequence seq = new VariableSequence(new DNASequence("ATCG"), new ArrayList<>());
        seq.addId("@SBL_ID:227:BCR1:CGCAGA:BCR2:ATCTGA/1");
        seq.addId("@SBL_ID:228:BCR1:CGCGGA:BCR2:ATCTAA/1");

        agg.addVariableSequence(seq);

        List<ComboAggregation> filtered = filter.filter(Collections.singletonList(agg), 2);

        assertEquals(1, filtered.get(0).getVariableSequences().get(0).getIds().size());

    }

    @Test
    void givenTwoReadsInSameComboWithThreeBPDifference_whenFilteredWith2BPMismatch_checkNoneRemoved() throws Exception{

        ComboAggregation agg = new ComboAggregation(tag, tag);
        VariableSequence seq = new VariableSequence(new DNASequence("ATCG"), new ArrayList<>());
        seq.addId("@SBL_ID:227:BCR1:CGCAGA:BCR2:AGCTGA/1");
        seq.addId("@SBL_ID:228:BCR1:CGCGGA:BCR2:ATCTAA/1");

        agg.addVariableSequence(seq);

        List<ComboAggregation> filtered = filter.filter(Collections.singletonList(agg), 2);

        assertEquals(2, filtered.get(0).getVariableSequences().get(0).getIds().size());

    }




}