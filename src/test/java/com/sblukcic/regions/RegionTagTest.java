package com.sblukcic.regions;

import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RegionTagTest {

    //test factories
    private RegionTagFactory rtFac = (name, sequence, rssBases) -> new RegionTag(
            name, new DNASequence(sequence), Locus.ALPHA, new DNASequence(rssBases),false
    );


    @Test
    void  nonEqualityTest() throws CompoundNotFoundException {

        RegionTag one = rtFac.createRegionTag("TAG_1", "ATCG", "TTTT");
        RegionTag two = rtFac.createRegionTag("TAG_2", "ATCG", "TTTT");

        assertNotEquals(one, two);

    }

    @Test
    void equalityTest() throws CompoundNotFoundException {

        RegionTag one = rtFac.createRegionTag("TAG_1", "ATCG", "TTTT");
        RegionTag two = rtFac.createRegionTag("TAG_1", "ATTT", "TCCC");

        assertEquals(one, two);

    }
}