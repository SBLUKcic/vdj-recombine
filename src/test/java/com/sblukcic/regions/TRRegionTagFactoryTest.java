package com.sblukcic.regions;

import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TRRegionTagFactoryTest {

    private TRRegionTagFactory factory;

    @BeforeEach
    void setUp() {
        factory = new TRRegionTagFactory();
    }

    @Test
    void creatingAlphaCombo() throws CompoundNotFoundException {

        RegionTag tag = factory.createRegionTag("TRA4", "ATCG", "GGGG");

        assertEquals("TRA4", tag.getName());
        assertEquals(Locus.ALPHA, tag.getLocus());
        assertEquals("ATCG", tag.getSequence().getSequenceAsString());
        assertEquals("GGGG", tag.getRssBases().getSequenceAsString());
        assertFalse(tag.isRepetitive());

    }

    @Test
    void creatingBetaCombo() throws CompoundNotFoundException {

        RegionTag tag = factory.createRegionTag("TRB4", "ATCG", "GGGG");

        assertEquals("TRB4", tag.getName());
        assertEquals(Locus.BETA, tag.getLocus());
        assertEquals("ATCG", tag.getSequence().getSequenceAsString());
        assertEquals("GGGG", tag.getRssBases().getSequenceAsString());
        assertFalse(tag.isRepetitive());

    }

    @Test
    void creatingGammaCombo() throws CompoundNotFoundException {

        RegionTag tag = factory.createRegionTag("TRG4", "ATCG", "GGGG");

        assertEquals("TRG4", tag.getName());
        assertEquals(Locus.GAMMA, tag.getLocus());
        assertEquals("ATCG", tag.getSequence().getSequenceAsString());
        assertEquals("GGGG", tag.getRssBases().getSequenceAsString());
        assertFalse(tag.isRepetitive());

    }

    @Test
    void creatingDeltaCombo() throws CompoundNotFoundException {

        RegionTag tag = factory.createRegionTag("TRD4", "ATCG", "GGGG");

        assertEquals("TRD4", tag.getName());
        assertEquals(Locus.ALPHA, tag.getLocus());
        assertEquals("ATCG", tag.getSequence().getSequenceAsString());
        assertEquals("GGGG", tag.getRssBases().getSequenceAsString());
        assertFalse(tag.isRepetitive());

    }

    @Test
    void creatingDeltaComboNotCaseSensitive() throws CompoundNotFoundException {

        RegionTag tag = factory.createRegionTag("Trd4", "ATCG", "GGGG");

        assertEquals("Trd4", tag.getName());
        assertEquals(Locus.ALPHA, tag.getLocus());
        assertEquals("ATCG", tag.getSequence().getSequenceAsString());
        assertEquals("GGGG", tag.getRssBases().getSequenceAsString());
        assertFalse(tag.isRepetitive());

    }

    @Test
    void createRepCombo() throws  CompoundNotFoundException {

        RegionTag tag = factory.createRegionTag("rTAG(TRAJ4/TRAJ5)", "ATCG", "GGGG");

        assertEquals("rTAG(TRAJ4/TRAJ5)", tag.getName());
        assertEquals(Locus.ALPHA, tag.getLocus());
        assertEquals("ATCG", tag.getSequence().getSequenceAsString());
        assertEquals("GGGG", tag.getRssBases().getSequenceAsString());
        assertTrue(tag.isRepetitive());
    }

    @Test
    void createRepNotCaseSensitive() throws  CompoundNotFoundException {

        RegionTag tag = factory.createRegionTag("rTag(TRAJ4/TRAJ5)", "ATCG", "GGGG");

        assertEquals("rTag(TRAJ4/TRAJ5)", tag.getName());
        assertEquals(Locus.ALPHA, tag.getLocus());
        assertEquals("ATCG", tag.getSequence().getSequenceAsString());
        assertEquals("GGGG", tag.getRssBases().getSequenceAsString());
        assertTrue(tag.isRepetitive());
    }

}