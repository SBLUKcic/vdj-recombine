package com.sblukcic.regions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class REGION_TAG_TYPETest {

    @Test
    void whenAlternativeTagsRequested_checkCorrectTagReturned() {

        assertEquals(REGION_TAG_TYPE.J_CODING_ALT, REGION_TAG_TYPE.getAlternative(REGION_TAG_TYPE.J_CODING));
        assertEquals(REGION_TAG_TYPE.V_CODING_ALT, REGION_TAG_TYPE.getAlternative(REGION_TAG_TYPE.V_CODING));
        assertEquals(REGION_TAG_TYPE.J_SIGNAL_ALT, REGION_TAG_TYPE.getAlternative(REGION_TAG_TYPE.J_SIGNAL));
        assertEquals(REGION_TAG_TYPE.V_SIGNAL_ALT, REGION_TAG_TYPE.getAlternative(REGION_TAG_TYPE.V_SIGNAL));

        //when already an alt, check just default returned.
        assertEquals(REGION_TAG_TYPE.V_SIGNAL_ALT, REGION_TAG_TYPE.getAlternative(REGION_TAG_TYPE.V_SIGNAL_ALT));
    }

    @Test
    void whenAlternativeTagTypeCheckedIfAlt_checkCorrect() {

        assertTrue(REGION_TAG_TYPE.isAltList(REGION_TAG_TYPE.V_SIGNAL_ALT));
        assertFalse(REGION_TAG_TYPE.isAltList(REGION_TAG_TYPE.V_SIGNAL));
    }
}