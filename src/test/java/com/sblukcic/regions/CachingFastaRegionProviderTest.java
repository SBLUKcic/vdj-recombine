package com.sblukcic.regions;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.when;

class CachingFastaRegionProviderTest {

    private RegionProvider regionProvider;

    @Mock
    private RegionReader regionReaderMock;

    @Mock
    private List<RegionTag> regionTagsMock;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void givenJRegionCodingRequested_whenParsed_checkCorrectRegionsReturned() throws  Exception{

        HashMap<REGION_TAG_TYPE, InputStream> locationMap = new HashMap<>();
        InputStream jCodingStream = getClass().getResourceAsStream("j-region-defaults_coding_test.fasta");
        InputStream jCodingStreamAlts = getClass().getResourceAsStream("j-region-defaults_coding_alt.fasta");
        locationMap.put(REGION_TAG_TYPE.J_CODING, jCodingStream);
        locationMap.put(REGION_TAG_TYPE.J_CODING_ALT, jCodingStreamAlts);
        regionProvider = new CachingFastaRegionProvider(locationMap, regionReaderMock);

        when(regionReaderMock.findRegionTags(jCodingStream, jCodingStreamAlts)).thenReturn(regionTagsMock);
        when(regionTagsMock.size()).thenReturn(111);
        List<RegionTag> jCodingTags  = regionProvider.getRegionsForTagType(REGION_TAG_TYPE.J_CODING);

        assertEquals(111, jCodingTags.size());

    }

    @Test
    void givenJRegionCodingRequested_whenParsedTwice_checkSameRegionsReturned() throws  Exception {

        HashMap<REGION_TAG_TYPE, InputStream> locationMap = new HashMap<>();
        InputStream jCodingStream = getClass().getResourceAsStream("j-region-defaults_coding_test.fasta");
        InputStream jCodingStreamAlts = getClass().getResourceAsStream("j-region-defaults_coding_alt.fasta");
        locationMap.put(REGION_TAG_TYPE.J_CODING, jCodingStream);
        locationMap.put(REGION_TAG_TYPE.J_CODING_ALT, jCodingStreamAlts);
        regionProvider = new CachingFastaRegionProvider(locationMap, regionReaderMock);

        when(regionReaderMock.findRegionTags(jCodingStream, jCodingStreamAlts)).thenReturn(regionTagsMock);
        when(regionTagsMock.size()).thenReturn(111);
        List<RegionTag> jCodingTags  = regionProvider.getRegionsForTagType(REGION_TAG_TYPE.J_CODING);

        assertEquals(111, jCodingTags.size());

        List<RegionTag> jCodingTagsAgain  = regionProvider.getRegionsForTagType(REGION_TAG_TYPE.J_CODING);

        assertEquals(111, jCodingTagsAgain.size());
        assertSame(jCodingTags, jCodingTagsAgain);

    }

    @Test
    void givenCodingMapRequested_whenParsed_checkCorrectRegionMapsReturned() throws  Exception {

        HashMap<REGION_TAG_TYPE, InputStream> locationMap = new HashMap<>();
        InputStream jCodingStream = getClass().getResourceAsStream("j-region-defaults_coding_test.fasta");
        InputStream jCodingStreamAlts = getClass().getResourceAsStream("j-region-defaults_coding_alt.fasta");
        locationMap.put(REGION_TAG_TYPE.J_CODING, jCodingStream);
        locationMap.put(REGION_TAG_TYPE.J_CODING_ALT, jCodingStreamAlts);
        regionProvider = new CachingFastaRegionProvider(locationMap, regionReaderMock);

        when(regionReaderMock.findRegionTags(jCodingStream, jCodingStreamAlts)).thenReturn(regionTagsMock);
        when(regionTagsMock.size()).thenReturn(111);

        Map<REGION_TAG_TYPE, List<RegionTag>> tags = regionProvider.getRegions();
        List<RegionTag> jCodingTags  = tags.get(REGION_TAG_TYPE.J_CODING);

        assertEquals(111, jCodingTags.size());

    }

    @Test
    void givenCodingMapRequested_whenParsedWithAllTags_checkCorrectRegionMapsReturned() throws  Exception {

        HashMap<REGION_TAG_TYPE, InputStream> locationMap = new HashMap<>();
        InputStream jCodingStream = getClass().getResourceAsStream("j-region-defaults_coding_test.fasta");
        InputStream jCodingStreamAlts = getClass().getResourceAsStream("j-region-defaults_coding_alt.fasta");
        locationMap.put(REGION_TAG_TYPE.J_CODING, jCodingStream);
        locationMap.put(REGION_TAG_TYPE.J_CODING_ALT, jCodingStreamAlts);
        locationMap.put(REGION_TAG_TYPE.J_SIGNAL, jCodingStream);
        locationMap.put(REGION_TAG_TYPE.J_SIGNAL_ALT, jCodingStreamAlts);
        locationMap.put(REGION_TAG_TYPE.V_CODING, jCodingStream);
        locationMap.put(REGION_TAG_TYPE.V_CODING_ALT, jCodingStreamAlts);
        locationMap.put(REGION_TAG_TYPE.V_SIGNAL, jCodingStream);
        locationMap.put(REGION_TAG_TYPE.V_SIGNAL_ALT, jCodingStreamAlts);
        regionProvider = new CachingFastaRegionProvider(locationMap, regionReaderMock);

        when(regionReaderMock.findRegionTags(jCodingStream, jCodingStreamAlts)).thenReturn(regionTagsMock);
        when(regionTagsMock.size()).thenReturn(111);

        Map<REGION_TAG_TYPE, List<RegionTag>> tags = regionProvider.getRegions();
        List<RegionTag> jCodingTags  = tags.get(REGION_TAG_TYPE.J_CODING);

        assertEquals(111, jCodingTags.size());

    }
}