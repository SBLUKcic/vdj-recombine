package com.sblukcic.regions;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FastaRegionReaderIntegrationTests {

    private RegionTagFactory factory;

    private FastaRegionReader reader;

    private String oneRecordStreamString = ">TRBJ2-6:RSS=CTCTGGGGCCAACGT\n" +
            "CCTGACTTTCGGGGCCGGCA";

    private String emptyStream = "";

    private String oneRecordRepeatStreamString = ">rTAG(TRGJ2rc/TRGJ1rc):RSS=GAATTATTATAAGAA\n" +
            "ACTCTTTGGCAGTGGAACAA";

    private String twoRecordRepeatStreamString = ">rTAG(TRGJ2rc/TRGJ1rc):RSS=GAATTATTATAAGAA\n" +
            "ACTCTTTGGCAGTGGAACAA\n" +
            ">rTAG(TRGJ3rc/TRGJ4rc):RSS=GAATTATTATAAGAA\n" +
            "ACTCTTTGGCAGTGGAACAA";

    private String altStreamString = ">TRGJ2rc_alt\n" +
            "TCCAAGGTAATAGAGGGAAG\n" +
            ">TRGJ1rc_alt\n" +
            "TCCAAGGTAATAGAGGGAAA\n" +
            ">TRGJ3rc_alt\n" +
            "TCCAAGGTAATAGAGGGAAG\n" +
            ">TRGJ4rc_alt\n" +
            "TCCAAGGTAATAGAGGGAAA\n";

    private String altStreamNonFunctionalNames = "@HELLO_alt\n" +
            "ATCTGTCTGTCTGTATGTG";

    private String twoRecordString = ">TRAJ61:RSS=GGTACCGGGTTAATA\n" +
            "GGAAACTGACATTTGGAGCC\n" +
            ">TRAJ60:RSS=TGAAGATCACCTAGA\n" +
            "TGCTCAACTTTGGGAAGGGG";


    @BeforeEach
    void setUp() {
        factory = new TRRegionTagFactory();
        reader = new FastaRegionReader(factory);
    }

    @Test
    void givenEmptyStreams_whenParsed_checkNothingReturned() throws  Exception {

        InputStream is = new ByteArrayInputStream(emptyStream.getBytes());
        InputStream alt = new ByteArrayInputStream(emptyStream.getBytes());

        List<RegionTag> tags = reader.findRegionTags(is, alt);

        assertEquals(0, tags.size());
    }

    @Test
    void givenOneReadNonRepetitive_whenParsed_checkCorrect() throws  Exception {

        InputStream is = new ByteArrayInputStream(oneRecordStreamString.getBytes());
        InputStream alt = new ByteArrayInputStream(emptyStream.getBytes());

        List<RegionTag> tags = reader.findRegionTags(is, alt);

        assertEquals(1, tags.size());
        RegionTag tag = tags.get(0);
        assertEquals("TRBJ2-6", tag.getName());
        assertEquals(Locus.BETA, tag.getLocus());
        assertEquals("CTCTGGGGCCAACGT", tag.getRssBases().getSequenceAsString());
        assertEquals("CCTGACTTTCGGGGCCGGCA", tag.getSequence().getSequenceAsString());
        assertEquals(0, tag.getAlternativeTags().size());

    }

    @Test
    void givenTwoReadsNonRepetitive_whenParsed_checkCorrect() throws  Exception {

        InputStream is = new ByteArrayInputStream(twoRecordString.getBytes());
        InputStream alt = new ByteArrayInputStream(emptyStream.getBytes());

        List<RegionTag> tags = reader.findRegionTags(is, alt);

        assertEquals(2, tags.size());

    }

    @Test
    void givenRepeatRecordWithoutAlt_whenParsed_checkValidButNoAlts() throws  Exception {

        InputStream is = new ByteArrayInputStream(oneRecordRepeatStreamString.getBytes());
        InputStream alt = new ByteArrayInputStream(emptyStream.getBytes());

        List<RegionTag> tags = reader.findRegionTags(is, alt);

        assertEquals(1, tags.size());
        RegionTag tag = tags.get(0);
        assertEquals("rTAG(TRGJ2rc/TRGJ1rc)", tag.getName());
        assertEquals(0, tag.getAlternativeTags().size());
    }

    @Test
    void givenRepeatRecordWithAlts_whenParsed_checkAltsParsedAlongside() throws  Exception {

        InputStream is = new ByteArrayInputStream(oneRecordRepeatStreamString.getBytes());
        InputStream alt = new ByteArrayInputStream(altStreamString.getBytes());

        List<RegionTag> tags = reader.findRegionTags(is, alt);

        assertEquals(1, tags.size());
        RegionTag tag = tags.get(0);
        assertEquals("rTAG(TRGJ2rc/TRGJ1rc)", tag.getName());
        assertEquals(2, tag.getAlternativeTags().size());
    }

    @Test
    void givenMultipleRepeatRecordsWithAlts_whenParsed_checkAltsParsedAlongside() throws  Exception {

        InputStream is = new ByteArrayInputStream(twoRecordRepeatStreamString.getBytes());
        InputStream alt = new ByteArrayInputStream(altStreamString.getBytes());

        List<RegionTag> tags = reader.findRegionTags(is, alt);

        assertEquals(2, tags.size());
        RegionTag tag = tags.get(0);
        assertEquals("rTAG(TRGJ2rc/TRGJ1rc)", tag.getName());
        assertEquals(2, tag.getAlternativeTags().size());
        RegionTag two = tags.get(1);
        assertEquals("rTAG(TRGJ3rc/TRGJ4rc)", two.getName());
        assertEquals(2, two.getAlternativeTags().size());
    }

    @Test
    void givenNullInputStreams_whenParsed_checkExceptionThrown()  throws  Exception{

        assertThrows(Exception.class, () -> reader.findRegionTags(null, null));

    }

    @Test
    void givenAltStreamHasNoSequence_whenParsed_checkExceptionThrown()  throws  Exception{
        InputStream main = new ByteArrayInputStream(twoRecordRepeatStreamString.getBytes());
        InputStream is = new ByteArrayInputStream(">someThing\n".getBytes());


        assertThrows(Exception.class, () -> reader.findRegionTags(main, is ));
    }

    @Test
    void givenMainStreamHasNoSequence_whenParsed_checkExceptionThrown()  throws  Exception{

        InputStream is = new ByteArrayInputStream(">someThing:RSS=ATCGTCTGTGT\n".getBytes());

        assertThrows(Exception.class,
                () -> reader.findRegionTags(is, new ByteArrayInputStream(altStreamString.getBytes())));

    }

    @Test
    void givenIncorrectAltNames_whenParsed_checkNoAltsRecognised()  throws  Exception{

        InputStream main = new ByteArrayInputStream(twoRecordRepeatStreamString.getBytes());
        InputStream alts  = new ByteArrayInputStream(altStreamNonFunctionalNames.getBytes());

        List<RegionTag> tags = reader.findRegionTags(main, alts);

        assertEquals(2, tags.size());
        RegionTag tag = tags.get(0);
        assertEquals("rTAG(TRGJ2rc/TRGJ1rc)", tag.getName());
        assertEquals(0, tag.getAlternativeTags().size());
        RegionTag two = tags.get(1);
        assertEquals("rTAG(TRGJ3rc/TRGJ4rc)", two.getName());
        assertEquals(0, two.getAlternativeTags().size());
    }
}