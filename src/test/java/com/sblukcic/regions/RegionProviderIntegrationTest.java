package com.sblukcic.regions;

import com.sblukcic.config.BeanConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegionProviderIntegrationTest {

    private static AnnotationConfigApplicationContext ctx;

    @BeforeEach
    void setUp() {
        ctx = new AnnotationConfigApplicationContext();
        ctx.register(BeanConfig.class);
        ctx.refresh();
    }

    @Test
    void givenDefaultBeanConfig_whenRegionsCalled_checkValidRegionsReturned() throws  Exception {

        RegionProvider provider = ctx.getBean(RegionProvider.class);

        Map<REGION_TAG_TYPE, List<RegionTag>> tagMap = provider.getRegions();

        List<RegionTag> jCoding = tagMap.get(REGION_TAG_TYPE.J_CODING);
        assertTrue(jCoding.size() > 0);

        List<RegionTag> vCoding = tagMap.get(REGION_TAG_TYPE.V_CODING);
        assertTrue(vCoding.size() > 0);

        List<RegionTag> jSignal = tagMap.get(REGION_TAG_TYPE.J_SIGNAL);
        assertTrue(jSignal.size() > 0);

        List<RegionTag> vSignal = tagMap.get(REGION_TAG_TYPE.V_SIGNAL);
        assertTrue(vSignal.size() > 0);
    }
}
