package com.sblukcic.algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoyerMooreMismatchAlgorithmTest {

    private SearchAlgorithm searcher;

    private final String longString = "I am a long string with something inside!";
    private final String longStringPattern = "long string";
    private final String longStringPatternOneMismatch = "lnng string";
    private final String longStringPatternFourMismatch = "lnog stirng";

    @Test
    void givenCreatedValidPattern_whenSearched_checkCorrect() {

        searcher = new BoyerMooreMismatchAlgorithm(longStringPattern, 0);
        int pos = searcher.search(longString);
        assertEquals(7, pos);
    }

    @Test
    void givenPatternLongerThanText_whenSearched_checkMinusOneReturned() {

        searcher = new BoyerMooreMismatchAlgorithm(longString, 0);
        int pos = searcher.search(longStringPattern);
        assertEquals(-1, pos);

    }

    @Test
    void givenValidPatternIncOneMismatch_whenSearched_checkMinusOneReturned() {

        searcher = new BoyerMooreMismatchAlgorithm(longStringPatternOneMismatch, 0);
        int pos = searcher.search(longString);
        assertEquals(-1, pos);
    }

    @Test
    void givenValidPatternIncOneMismatch_whenSearchedWithMismatchAllowed_checkCorrect() {

        searcher = new BoyerMooreMismatchAlgorithm(longStringPatternOneMismatch, 1);
        int pos = searcher.search(longString);
        assertEquals(7, pos);
    }

    @Test
    void givenValidPatternIncFourMismatch_whenSearchedWithThreeMismatchAllowed_checkCorrect() {

        searcher = new BoyerMooreMismatchAlgorithm(longStringPatternFourMismatch, 3);
        int pos = searcher.search(longString);
        assertEquals(-1, pos);
    }

    @Test
    void givenValidPatternIncFourMismatch_whenSearchedWithFourMismatchAllowed_checkCorrect() {

        searcher = new BoyerMooreMismatchAlgorithm(longStringPatternFourMismatch, 4);
        int pos = searcher.search(longString);
        assertEquals(7, pos);
    }
}