package com.sblukcic.algorithms;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoyerMooreMismatchAlgorithmFactoryTest {

    private SearchAlgorithm searcher;
    private MismatchAwareAlgorithmFactory factory;

    private final String longString = "I am a long string with something inside!";

    @BeforeEach
    void setUp() {
        factory = new BoyerMooreMismatchAlgorithmFactory();
    }

    @Test
    void givenDefaultBoyerMooreReturned_whenSearched_checkCorrect() {

        String longStringPattern = "long string";
        searcher = factory.createSearchAlgorithm(longStringPattern);
        int pos = searcher.search(longString);
        assertEquals(7, pos);
    }

    @Test
    void givenBoyerMooreWithOneMismatchReturned_whenSearched_checkCorrect() {

       factory.setAllowedMismatches(1);
        String longStringPatternOneMismatch = "lnng string";
        searcher = factory.createSearchAlgorithm(longStringPatternOneMismatch);
        int pos = searcher.search(longString);
        assertEquals(7, pos);
        assertEquals(1, factory.getAllowedMismatches());
    }

    @Test
    void givenBoyerMooreWithFourMismatchReturned_whenSearched_checkCorrect() {

        factory.setAllowedMismatches(4);
        String longStringPatternFourMismatch = "lnog stirng";
        searcher = factory.createSearchAlgorithm(longStringPatternFourMismatch);
        int pos = searcher.search(longString);
        assertEquals(7, pos);
        assertEquals(4, factory.getAllowedMismatches());
    }
}