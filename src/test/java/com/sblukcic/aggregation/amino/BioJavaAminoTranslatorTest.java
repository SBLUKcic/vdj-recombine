package com.sblukcic.aggregation.amino;

import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.transcription.Frame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BioJavaAminoTranslatorTest {

    private AminoTranslator translator;

    @BeforeEach
    void setUp() {
        translator = new BioJavaAminoTranslator();
    }

    @Test
    void givenValidStringWithNoValidTranslations_whenTranslated_checkNoProductiveAminos() throws CompoundNotFoundException {

       List<Amino> aminos =  translator.translate(new DNASequence("ATCGTTAATCGCGTGTGTGCTGCTGAT"),
               new DNASequence("TACTCTATCGG"),
               new DNASequence("GGGTGTGTGTC"));

       assertEquals(3, aminos.size());
       boolean isProductive = false;
       for(Amino amino : aminos){
           if(amino.isProductive()){
               isProductive = true;
           }
       }

       assertFalse(isProductive);
    }

    @Test
    void givenValidStringWithValidTranslations_whenTranslated_checkNoProductiveAminos() throws CompoundNotFoundException {

        List<Amino> aminos =  translator.translate(new DNASequence("GCCACCTGGGATAGGGGATATAGTAGTGATTGGA"),
                new DNASequence("ACTCTGGGGTCTATTACTGT"),
                new DNASequence("TCAAGACGTTTGCAAAAGGG"));

        assertEquals(3, aminos.size());
        boolean isProductive = false;
        for(Amino amino : aminos){
            if(amino.isProductive()){
                isProductive = true;
            }
        }

        assertTrue(isProductive);
    }
}