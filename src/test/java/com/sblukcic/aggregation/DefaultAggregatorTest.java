package com.sblukcic.aggregation;

import com.sblukcic.aggregation.amino.AminoTranslator;
import com.sblukcic.combinations.RegionCombination;
import com.sblukcic.combinations.RegionCombinationFactory;
import com.sblukcic.regions.Locus;
import com.sblukcic.regions.RegionTag;
import com.sblukcic.regions.RegionTagFactory;
import org.biojava.nbio.core.sequence.DNASequence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class DefaultAggregatorTest {

    private Aggregator aggregator;

    @Mock
    private AminoTranslator translator;

    //test factories
    private RegionTagFactory factory = (name, sequence, rssBases) -> new RegionTag(
            name, new DNASequence(sequence), Locus.ALPHA, new DNASequence(rssBases),false
    );

    private RegionCombinationFactory regionCombinationFactory = (one, two, sequence, id) ->
            new RegionCombination(one, two, new DNASequence(sequence), id);

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        when(translator.translate(any(), any(), any())).thenReturn(new ArrayList<>());
        aggregator = new DefaultAggregator(translator);
    }

    @Test
    void given3RegionCombosWithSameTags_whenAggregated_checkCorrect() throws Exception {

        RegionTag one = factory.createRegionTag("ONE", "ATCG", "TTGC");
        RegionTag two = factory.createRegionTag("TWO", "TTGC", "ATCG");

        RegionCombination cOne = regionCombinationFactory.create(one, two, "AAAA", "@ID1");
        RegionCombination cTwo = regionCombinationFactory.create(one, two, "AAAA", "@ID2");
        RegionCombination cThree = regionCombinationFactory.create(one, two, "ATCG", "@ID3");
        RegionCombination cThreeRC = regionCombinationFactory.create(one, two, "CGAT", "@ID4");

        List<RegionCombination> list = Arrays.asList(cOne, cTwo, cThree, cThreeRC);

        List<ComboAggregation> aggregations = aggregator.aggregate(list);

        assertEquals(1, aggregations.size());
        ComboAggregation agg = aggregations.get(0);
        assertEquals(2, agg.getVariableSequences().size());
        assertEquals("AAAA", agg.getVariableSequences().get(0).getSequence().getSequenceAsString());
        assertEquals(2, agg.getVariableSequences().get(0).getIds().size());
        assertEquals("ATCG", agg.getVariableSequences().get(1).getSequence().getSequenceAsString());
        assertEquals(2, agg.getVariableSequences().get(1).getIds().size());

    }
}